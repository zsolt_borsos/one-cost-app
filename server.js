var express = require('express');
var app = express();

//set environment
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
//create app

//load config
var config = require('./server/config/config')[env];
//load express settings
require('./server/config/express')(app, config);
//load mongoose settings - mongoDb module
require('./server/config/mongoose')(config);
//load passport - login module
require('./server/config/passport')();
//load mysql - mysql db module
require('./server/config/mysql');
//load xlsx - xlsx reader module
require('./server/config/xlsx');

//load routes - custom routes for the app
require('./server/config/routes')(app);

//creating a module from the app
module.exports = app;
