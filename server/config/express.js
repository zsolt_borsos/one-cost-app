var express = require('express'),
    stylus = require('stylus'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    passport = require('passport'),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    fs = require('fs');
    //var ocimporter = require('oc-importer');


module.exports = function(app, config) {

    function compile(str, path) {
      return stylus(str)
        .set('filename', path);
    }

    app.set('views', config.rootPath + '/server/views');
    app.set('view engine', 'jade');
    app.use(logger('dev'));
    app.use(cookieParser());

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());

    app.use(session({secret: 'oc unicorns',resave:false,saveUninitialized:false}));

    app.use(passport.initialize());
    app.use(passport.session());


    app.use(stylus.middleware(
      {
        src: config.rootPath + '/public',
        compile: compile
      }
    ));


    app.use(express.static(config.rootPath + '/public'));


    //multer config for invoices
    var invoiceUploader = multer(
      {
        dest: './uploads/',
        changeDest: function(dest, req, res) {
          //get ccId from post body
          var id = req.body.ccId;
          //create path with id and default dest
          var path = dest + id;
          var stat = null;

          try {
              // using fs.statSync; NOTE that fs.existsSync is now deprecated; fs.accessSync could be used but is only nodejs >= v0.12.0
              stat = fs.statSync(path);
          } catch(err) {
              // for nested folders, look at npm package "mkdirp"
              fs.mkdirSync(path);
          }

          if (stat && !stat.isDirectory()) {
              // Woh! This file/link/etc already exists, so isn't a directory. Can't save in it. Handle appropriately.
              throw new Error('Directory cannot be created because an inode of a different type exists at "' + dest + '"');
          }
          return path;

        },
        rename: function (fieldname, filename) {
          return filename.replace(/\W+/g, '-').toLowerCase() + '-' +Date.now();
        },
        putSingleFilesInArray: true,

        onFileUploadStart: function (file, req, res) {
          console.log(file.fieldname + ' is starting ...');
        },

        onFileUploadComplete: function (file, req, res) {
          console.log(file.fieldname + ' uploaded to  ' + file.path);
        },
        onError: function (error, next) {
          console.log(error);
          next(error);
        }
      }
    );

  app.invoiceUploader = invoiceUploader;



  /* for below if needed!
  changeDest: function(dest, req, res) {

    var stat = null;

    try {
        // using fs.statSync; NOTE that fs.existsSync is now deprecated; fs.accessSync could be used but is only nodejs >= v0.12.0
        stat = fs.statSync(dest);
    } catch(err) {
        // for nested folders, look at npm package "mkdirp"
        fs.mkdirSync(dest);
    }

    if (stat && !stat.isDirectory()) {
        // Woh! This file/link/etc already exists, so isn't a directory. Can't save in it. Handle appropriately.
        throw new Error('Directory cannot be created because an inode of a different type exists at "' + dest + '"');
    }
    return dest;

  },
  */
  //multer config for sales
  var salesUploader = multer(
    {
      dest: './uploads/sales/',

      rename: function (fieldname, filename) {
        return filename.replace(/\W+/g, '-').toLowerCase() + '-' +Date.now();
      },
      putSingleFilesInArray: true,

      onFileUploadStart: function (file, req, res) {
        console.log(file.fieldname + ' is starting ...');
      },

      onFileUploadComplete: function (file, req, res) {
        console.log(file.fieldname + ' uploaded to  ' + file.path);
        req.uploadedFile = file.path;
        //console.log('Woohoo we are nearly there!');

      },
      onError: function (error, next) {
        console.log(error);
        next(error);
      }
    }
  );

app.salesUploader = salesUploader;


}
