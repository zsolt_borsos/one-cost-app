var path = require('path');
var rootPath = path.normalize(__dirname + '/../../');

module.exports = {

    development: {
      mdb: 'mongodb://localhost/onecostapp',
      rootPath: rootPath,
      port: process.env.PORT || 3030
    },
    production: {
      mdb: 'mongodb://onecostapp:ocdbcon@ds062797.mongolab.com:62797/onecostappdb',
      rootPath: rootPath,
      port: process.env.PORT || 80
    }

}
