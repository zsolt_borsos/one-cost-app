var mysql = require('mysql');

//creating a pool for db connection management
var pool = mysql.createPool({
	    connectionLimit : 80,
	    host     : 'localhost',
	    user     : 'root',
	    password : '',
	    database : 'onecost'
		});

//message if there are too many open connections
pool.on('enqueue', function () {
  console.log('Waiting for available connection slot...');
});
//create a global variable for the pool so it can be accessed anywhere.
//This might need changing later for security?
global.pool = pool;
