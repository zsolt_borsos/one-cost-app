var auth = require('./auth'),
    users = require('../controllers/users'),
    clients = require('../controllers/clients'),
    clientgroups = require('../controllers/clientgroups'),
    industries = require('../controllers/industries'),
    locations = require('../controllers/locations'),
    counties = require('../controllers/counties'),
    ccs = require('../controllers/ccs'),
    invoices = require('../controllers/invoices'),
    invoiceItems = require('../controllers/invoiceItems'),
    products = require('../controllers/products'),
    suppliers = require('../controllers/suppliers'),
    productDetails = require('../controllers/productDetails'),
    measures = require('../controllers/measureTypes'),
    prices = require('../controllers/prices'),
    productMatches = require('../controllers/productMatches'),
    mongoose = require('mongoose'),
    importer = require('../controllers/importer'),
    multer = require('multer'),
    User = mongoose.model('User');

module.exports = function(app) {


    //serve partials from angular dir
    app.get('/partials/*', function(req, res) {
        res.render('../../public/app/' + req.params[0]);
    });

    //general application routes
    app.get('/api/users', auth.requiresRole('admin'), users.getUsers);
    app.post('/api/users', users.createUser);
    app.put('/api/users', users.updateUser);

    app.post('/login', auth.authenticate);
    app.post('/logout', function(req, res){
      req.logout();
      res.end();
    });


    //business API routes
    //these will need login check added later!

    //clients
    app.get('/api/v1/clients', clients.getClients);
    app.get('/api/v1/clients/:id', clients.getClient);
    app.post('/api/v1/clients', clients.addClient);
    app.delete('/api/v1/clients/:id', clients.deleteClient);
    app.put('/api/v1/clients/:id', clients.updateClient);

    //client groups
    app.get('/api/v1/clientgroups', clientgroups.getClientGroups);
    app.get('/api/v1/clientgroups/:id', clientgroups.getClientGroup);
    app.post('/api/v1/clientgroups', clientgroups.addClientGroup);
    app.delete('/api/v1/clientgroups/:id', clientgroups.deleteClientGroup);
    app.put('/api/v1/clientgroups/:id', clientgroups.updateClientGroup);

    //industry
    app.get('/api/v1/industries/:id', industries.getIndustry);
    app.get('/api/v1/industries', industries.getIndustries);
    app.post('/api/v1/industries', industries.addIndustry);
    app.delete('/api/v1/industries/:id', industries.deleteIndustry);
    app.put('/api/v1/industries/:id', industries.updateIndustry);

    //location
    app.get('/api/v1/locations/:id', locations.getLocation);
    app.get('/api/v1/locations', locations.getLocations);
    app.post('/api/v1/locations', locations.addLocation);
    app.delete('/api/v1/locations/:id', locations.deleteLocation);
    app.put('/api/v1/locations/:id', locations.updateLocation);

    //county
    app.get('/api/v1/counties/:id', counties.getCounty);
    app.get('/api/v1/counties', counties.getCounties);
    app.post('/api/v1/counties', counties.addCounty);
    app.delete('/api/v1/counties/:id', counties.deleteCounty);
    app.put('/api/v1/counties/:id', counties.updateCounty);

    //ccs
    app.get('/api/v1/ccs', ccs.getCCs);
    app.get('/api/v1/ccs/:id', ccs.getCC);
    app.post('/api/v1/ccs', ccs.addCC);
    app.delete('/api/v1/ccs/:id', ccs.deleteCC);
    app.put('/api/v1/ccs/:id', ccs.updateCC);

    app.get('/api/v1/invoices', invoices.getInvoices);
    app.get('/api/v1/invoices/:id', invoices.getInvoice);
    app.get('/api/v1/invoices/:id/image', invoices.getInvoiceImage);
    //using multer middleware for file upload
    app.post('/api/v1/invoices', app.invoiceUploader, invoices.addInvoice);
    app.delete('/api/v1/invoices/:id', invoices.deleteInvoice);
    app.put('/api/v1/invoices/:id', invoices.updateInvoice);

    app.get('/api/v1/products', products.getProducts);
    app.get('/api/v1/products/:id', products.getProduct);
    app.post('/api/v1/products', products.addProduct);
    app.delete('/api/v1/products/:id', products.deleteProduct);
    app.put('/api/v1/products/:id', products.updateProduct);

    //products for suppliers
    app.get('/api/v1/suppliers/:supplierId/products', products.getProductsBySupplier);
    app.get('/api/v1/suppliers/:supplierId/productscount', products.getProductsBySupplierCount);
    //app.get('/api/v1/suppliers/:supplierId/products/:id', products.getProductBySupplier);
    //app.post('/api/v1/suppliers/:supplierId/products', products.addProductBySupplier);
    //app.delete('/api/v1/suppliers/:supplierId/products/:id', products.deleteProduct);
    //app.put('/api/v1/suppliers/:supplierId/products/:id', products.updateProduct);

    app.get('/api/v1/suppliers', suppliers.getSuppliers);
    app.get('/api/v1/suppliers/:id', suppliers.getSupplier);
    app.post('/api/v1/suppliers', suppliers.addSupplier);
    app.delete('/api/v1/suppliers/:id', suppliers.deleteSupplier);
    app.put('/api/v1/suppliers/:id', suppliers.updateSupplier);

    app.get('/api/v1/invoiceItems', invoiceItems.getInvoiceItems);
    app.get('/api/v1/invoiceItems/:id', invoiceItems.getInvoiceItem);
    app.post('/api/v1/invoiceItems', invoiceItems.addInvoiceItem);
    app.delete('/api/v1/invoiceItems/:id', invoiceItems.deleteInvoiceItem);
    app.put('/api/v1/invoiceItems/:id', invoiceItems.updateInvoiceItem);

    app.get('/api/v1/productDetails', productDetails.getProductDetails);
    app.get('/api/v1/productDetails/:id', productDetails.getProductDetail);
    app.post('/api/v1/productDetails', productDetails.addProductDetail);
    app.delete('/api/v1/productDetails/:id', productDetails.deleteProductDetail);
    app.put('/api/v1/productDetails/:id', productDetails.updateProductDetail);

    app.get('/api/v1/measures', measures.getMeasureTypes);
    app.get('/api/v1/measures/:id', measures.getMeasureType);
    app.post('/api/v1/measures', measures.addMeasureType);
    app.delete('/api/v1/measures/:id', measures.deleteMeasureType);
    app.put('/api/v1/measures/:id', measures.updateMeasureType);

    app.get('/api/v1/prices', prices.getPrices);
    app.get('/api/v1/prices/:id', prices.getPrice);
    app.post('/api/v1/prices', prices.addPrice);
    app.delete('/api/v1/prices/:id', prices.deletePrice);
    app.put('/api/v1/prices/:id', prices.updatePrice);

    app.get('/api/v1/productMatch', productMatches.getProductMatches);
    app.get('/api/v1/productMatch/:id', productMatches.getProductMatch);
    app.post('/api/v1/productMatch', productMatches.addProductMatch);
    app.delete('/api/v1/productMatch/:pid/:iid', productMatches.deleteProductMatch);
    app.put('/api/v1/productMatch/:id', productMatches.updateProductMatch);

    app.post('/api/v1/import/sales', app.salesUploader, importer.uploadSalesFile);
    //app.post('/api/v1/import/priceFile', app.priceFileUpload, importer.priceFileUpload);

    // defaults / error handling
    app.all('/api/*', function(req,res){
      res.sendStatus(404);
    });

    app.get('*', function(req, res){
      res.render('index', {
          bootstrappedUser: req.user
        });
    });
}
