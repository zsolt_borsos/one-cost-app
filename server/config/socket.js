
module.exports = function(io){

	io.on('connection', function(socket){
  	console.log('a user connected to the app!');

		socket.on('disconnect', function(){
    	console.log('user disconnected');
  	});

		socket.on('updateInvoices', function(){
			//console.log('Got msg from client to update Invoices.');
			socket.broadcast.emit('updateInvoices');
		});

		socket.on('updateCC', function(){
			//console.log('Got msg from client to update CCs');
			socket.broadcast.emit('updateCC');
		});

		socket.on('globalNotify', function(msg){
			socket.broadcast.emit('globalNotify', msg);
		});

		socket.on('globalError', function(msg){
			socket.broadcast.emit('globalError', msg);
		});

		socket.on('spinnerStart', function(){
			socket.emit('spinnerStart');
		});

		socket.on('spinnerStop', function(){
			socket.emit('spinnerStop');
		});

		socket.on('updateClients', function(){
			socket.broadcast.emit('updateClients');
		});
		socket.on('updateGroups', function(){
			socket.broadcast.emit('updateGroups');
		});
		socket.on('updateSuppliers', function(){
			socket.broadcast.emit('updateSuppliers');
		});
		socket.on('updateLocations', function(){
			socket.broadcast.emit('updateLocations');
		});
		socket.on('updateIndustries', function(){
			socket.broadcast.emit('updateIndustries');
		});
		socket.on('updateSuppliers', function(){
			io.emit('updateSuppliers');
		});

		socket.on('updateProducts', function(){
			io.emit('updateProducts');
		});

		socket.on('updateInvoiceItems', function(){
			//console.log('Sending signal to update invoice items!');
			io.emit('updateInvoiceItems');
		});

		socket.on('selectedProduct', function(product){
			socket.emit('setSelectedProduct', product);
		});

		socket.on('updateProductListForInvoiceManager', function(){
			io.emit('updateProductListForInvoiceManager');
		});

		/*
		socket.on('selectedItemToMatch', function(item){
			socket.emit('selectedItemToMatch');
		});
		*/

		/*
		socket.on('addedMeasureDetails', function(details){
			socket.emit('addMeasureDetails', details);
		});
		*/

	});
};
