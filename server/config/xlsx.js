var xlsx = require('xlsx');

function readFileData(filename) {

    var workbook = xlsx.readFile(filename);
    var sheet_name_list = workbook.SheetNames;
    //creating array for products
    var fileData = [];

    //iterate list of sheets/allow to import multiple worksheets? maybe future feature
    sheet_name_list.forEach(function(name) {
        var worksheet = workbook.Sheets[name];
        fileData = xlsx.utils.sheet_to_json(worksheet);
    });
    return fileData;
}

module.exports = readFileData;
