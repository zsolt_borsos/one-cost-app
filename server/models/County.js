
function getCounties(callback) {

	var sql = 'SELECT * FROM county';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function getCounty(id, callback) {

	var sql = 'SELECT * FROM county WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function addCounty(details, callback) {

	var sql = 'INSERT INTO county (name) VALUES (?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.name], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deleteCounty(id, callback) {

	var sql = 'DELETE FROM county WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateCounty(details, callback) {

	var sql = 'UPDATE county SET name = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.name, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


exports.getCounty = getCounty;
exports.getCounties = getCounties;
exports.addCounty = addCounty;
exports.deleteCounty = deleteCounty;
exports.updateCounty = updateCounty;
