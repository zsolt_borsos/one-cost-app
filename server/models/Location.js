
function getLocations(callback) {

	var sql = 'SELECT * FROM location';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function getLocation(id,callback) {

	var sql = 'SELECT * FROM location WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function addLocation(details, callback) {

	var sql = 'INSERT INTO location (name, note, countyId) VALUES (?, ?, ?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.name, details.note, details.countyId], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deleteLocation(id, callback) {

	var sql = 'DELETE FROM location WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateLocation(details, callback) {

	var sql = 'UPDATE location SET name = ?, note = ?, countyId = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.name, details.note, details.countyId, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};



exports.getLocations = getLocations;
exports.getLocation = getLocation;
exports.addLocation = addLocation;
exports.deleteLocation = deleteLocation;
exports.updateLocation = updateLocation;
