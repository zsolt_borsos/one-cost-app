
function getClientGroups(callback) {

	var sql = 'SELECT * FROM clientGroup';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function getClientGroup(id, callback) {

	var sql = 'SELECT * FROM clientGroup WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function addClientGroup(details, callback) {

	var sql = 'INSERT INTO clientGroup (name, note) VALUES (?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.name, details.note], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deleteClientGroup(id, callback) {

	var sql = 'DELETE FROM clientGroup WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateClientGroup(details, callback) {

	var sql = 'UPDATE clientGroup SET name = ?, note = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.name, details.note, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


exports.getClientGroup = getClientGroup;
exports.getClientGroups = getClientGroups;
exports.addClientGroup = addClientGroup;
exports.deleteClientGroup = deleteClientGroup;
exports.updateClientGroup = updateClientGroup;
