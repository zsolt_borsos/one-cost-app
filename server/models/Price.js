
function getPrices(callback) {

	var sql = 'SELECT * FROM price';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function getPrice(id,callback) {

	var sql = 'SELECT * FROM price WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function getPriceByInvoiceItemId(id, callback) {

	var sql = 'SELECT * FROM price WHERE sourceValue = ? AND sourceTypeId = 3';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function getPriceByProductId(id,callback) {

	var sql = 'SELECT * FROM price WHERE productId = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function getPriceByType(typeId, callback) {
	var sql = 'SELECT * FROM price WHERE sourceTypeId = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[typeId], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			console.log(results);
			callback(null, results);
		});
	});
};


function addPrice(details, callback) {

	var myDate = Date_toYMD(details.dateFrom);
	details.dateFrom = myDate;

	var sql = 'INSERT INTO price (productId, dateFrom, `value`, sourceTypeId, sourceValue, note) VALUES (?,?,?,?,?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.productId, details.dateFrom, details.value, details.sourceTypeId, details.sourceValue, details.note], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deletePrice(id, callback) {

	var sql = 'DELETE FROM price WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updatePrice(details, callback) {

	var myDate = Date_toYMD(details.dateFrom);
	details.dateFrom = myDate;

	var sql = 'UPDATE price SET productId = ?, dateFrom = ?, `value` = ?, sourceTypeId = ?, sourceValue = ?, note = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.productId, details.dateFrom, details.value, details.sourceTypeId, details.sourceValue, details.note, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function Date_toYMD(d) {
	d = new Date(d);
  var year, month, day;
  year = String(d.getFullYear());
  month = String(d.getMonth() + 1);
  if (month.length == 1) {
      month = "0" + month;
  }
  day = String(d.getDate());
  if (day.length == 1) {
      day = "0" + day;
  }
  return year + "-" + month + "-" + day;
}

exports.getPriceByInvoiceItemId = getPriceByInvoiceItemId;
exports.getPriceByProductId = getPriceByProductId;
exports.getPriceByType = getPriceByType;
exports.getPrices = getPrices;
exports.getPrice = getPrice;
exports.addPrice = addPrice;
exports.deletePrice = deletePrice;
exports.updatePrice = updatePrice;
