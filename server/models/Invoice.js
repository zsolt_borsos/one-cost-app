
function getInvoices(callback) {

	var sql = 'SELECT * FROM invoice';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};


function getInvoice(id, callback) {

	var sql = 'SELECT * FROM invoice WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};


function addInvoice(details, callback) {

	if (!details.ccId) {
		callback('No ccId!');
	}

	console.log('Invoice date:', details.date);
	if (!details.date) {
		details.date = '0000-00-00';
	}else{
		var myDate = Date_toYMD(details.date);
		details.date = myDate;
		console.log('Converted date:', details.date);
	}

	var sql = 'INSERT INTO invoice (date, ccId, note, imageName) VALUES (?,?,?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.date, details.ccId, details.note, details.imageName], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deleteInvoice(id, callback) {

	var sql = 'DELETE FROM invoice WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateInvoice(details, callback) {


	if (!details.ccId) {
		callback('No ccId!');
	}
	console.log('Invoice date:', details.date);
	if (!details.date) {
		details.date = '0000-00-00';
	}else {
		var myDate = Date_toYMD(details.date);
		details.date = myDate;
		console.log('Converted date:', details.date);

	}

	var sql = 'UPDATE invoice SET date = ?, note = ?, total = ?, supplierId = ?, done = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.date, details.note, details.total, details.supplierId, details.done, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


function Date_toYMD(d) {
	d = new Date(d);
  var year, month, day;
  year = String(d.getFullYear());
  month = String(d.getMonth() + 1);
  if (month.length == 1) {
      month = "0" + month;
  }
  day = String(d.getDate());
  if (day.length == 1) {
      day = "0" + day;
  }
  return year + "-" + month + "-" + day;
}

exports.getInvoice = getInvoice;
exports.getInvoices = getInvoices;
exports.addInvoice = addInvoice;
exports.deleteInvoice = deleteInvoice;
exports.updateInvoice = updateInvoice;
