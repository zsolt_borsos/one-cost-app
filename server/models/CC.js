
function getCCs(callback) {

	var sql = 'SELECT * FROM cc';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};


function getCC(id, callback) {

	var sql = 'SELECT * FROM cc WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [id], function(err, results) {
			//console.log(err);
			//console.log(results);
			if (err) {
				callback(err);
			}

			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function addCC(details, callback) {

	var currentDate = new Date();
	var sql = 'INSERT INTO cc (clientId, note, date, userId) VALUES (?,?,?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.clientId, details.note, new Date(), details.userId], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deleteCC(id, callback) {

	var sql = 'DELETE FROM cc WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateCC(details, callback) {

	var sql = 'UPDATE cc SET clientId = ?, note = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.clientId, details.note, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


exports.getCC = getCC;
exports.getCCs = getCCs;
exports.addCC = addCC;
exports.deleteCC = deleteCC;
exports.updateCC = updateCC;
