
function getProducts(callback) {

	var sql = 'SELECT * FROM product';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function getProductsByDescription(desc, callback) {
var formattedText = '%' + desc + '%';
var sql = "SELECT p.*, pd.description, pd.packsize, s.name FROM product AS p JOIN productDetails AS pd ON p.id = pd.productId JOIN supplier AS s ON s.id = p.supplierId WHERE pd.description LIKE ?";
	//console.log('sql:' + sql);
	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[formattedText], function(err, results) {
			if (err) {
				callback(err);
			}
			//console.log('GetProductByDesc result:', results);
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function getProductsByCode(code, callback) {

var formattedText = '%' + code + '%';
var sql = 'SELECT p.*, pd.description, pd.packsize, s.name FROM product AS p JOIN productDetails AS pd ON p.id = pd.productId JOIN supplier AS s ON s.id = p.supplierId WHERE p.productCode LIKE ?';
	//console.log('sql:' + sql);
	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [formattedText], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function getProductsBySupplierCount(details, callback) {

	var sql = 'SELECT COUNT(*) AS count FROM product WHERE supplierId = ' + details.id;

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};


function getProductsBySupplier(details, callback) {

	var offset = (details.page * details.limit) - details.limit;

	//var sql = 'SELECT * FROM product WHERE supplierId = ' + details.id + ' ORDER BY id ASC LIMIT ' + offset + ', ' + details.limit;
	var sql = 'SELECT * FROM product WHERE supplierId = ' + details.id + ' ORDER BY id ASC';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};


function getProduct(id, callback) {

	var sql = 'SELECT * FROM product WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};


function addProduct(details, callback) {

	var sql = 'INSERT INTO product (productCode, productCategoryId, supplierId, unitOfSale, measureValue, measureTypeId, note) VALUES (?,?,?,?,?,?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.productCode, details.productCategoryId, details.supplierId, details.unitOfSale, details.measureValue, details.measureTypeId, details.note], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			//console.log(results);
			callback(null, results);
		});
	});

};



function updateProduct(details, callback) {

	var sql = 'UPDATE product SET productCode = ?, productCategoryId = ?, supplierId = ?, unitOfSale = ?, measureValue = ?, measureTypeId = ?, note = ?  WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.productCode, details.productCategoryId, details.supplierId, details.unitOfSale, details.measureValue, details.measureTypeId, details.note, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function deleteProduct(id, callback) {

	var sql = 'DELETE FROM product WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


exports.getProductsByCode = getProductsByCode;
exports.getProductsByDescription = getProductsByDescription;
exports.getProductsBySupplierCount = getProductsBySupplierCount;
exports.getProductsBySupplier = getProductsBySupplier;
exports.getProduct = getProduct;
exports.getProducts = getProducts;
exports.addProduct = addProduct;
exports.deleteProduct = deleteProduct;
exports.updateProduct = updateProduct;
