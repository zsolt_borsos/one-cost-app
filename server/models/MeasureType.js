
function getMeasureTypes(callback) {

	var sql = 'SELECT * FROM measureType';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};


function getMeasureType(id, callback) {

	var sql = 'SELECT * FROM measureType WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};


function addMeasureType(details, callback) {

	var sql = 'INSERT INTO measureType (name, note) VALUES (?, ?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.name, details.note], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deleteMeasureType(id, callback) {

	var sql = 'DELETE FROM measureType WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateMeasureType(details, callback) {

	var sql = 'UPDATE measureType SET name = ?, note = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.name, details.note, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};



exports.getMeasureType = getMeasureType;
exports.getMeasureTypes = getMeasureTypes;
exports.addMeasureType = addMeasureType;
exports.deleteMeasureType = deleteMeasureType;
exports.updateMeasureType = updateMeasureType;
