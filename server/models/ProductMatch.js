
function getProductMatches(callback) {

	var sql = 'SELECT * FROM productMatch';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};


function getProductMatch(id, callback) {

	var sql = 'SELECT * FROM productMatch WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};


function getProductMatchByInvoiceItemId(id, callback) {

	var sql = 'SELECT * FROM productMatch WHERE invoiceItemId = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function addProductMatch(details, callback) {

	var sql = 'INSERT INTO productMatch (invoiceItemId, productId, priceId, qty, final, note) VALUES (?,?,?,?,0,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.invoiceItemId, details.productId, details.priceId, details.qty, details.note], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deleteProductMatch(id, callback) {

	var sql = 'DELETE FROM productMatch WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateProductMatch(details, callback) {


	var sql = 'UPDATE productMatch SET invoiceItemId = ?, productId = ?, priceId = ?, qty = ?, final = ?, note = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.invoiceItemId, details.productId, details.priceId, details.qty, details.final, details.note, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


exports.getProductMatchByInvoiceItemId = getProductMatchByInvoiceItemId;
exports.getProductMatch = getProductMatch;
exports.getProductMatches = getProductMatches;
exports.addProductMatch = addProductMatch;
exports.deleteProductMatch = deleteProductMatch;
exports.updateProductMatch = updateProductMatch;
