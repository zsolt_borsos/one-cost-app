
function getClients(callback) {
	/*
	var sql = 'SELECT c.id, c.name AS clientName, c.levelOfInterest, c.note, cg.name AS groupName, '
 						+ ' i.name AS industry, a.name AS area, l.name AS location, co.name AS county'
						+ ' FROM client AS c'
						+ ' LEFT JOIN clientGroup AS cg ON c.clientGroupId = cg.id'
						+ ' LEFT JOIN industry AS i ON c.industryId = i.id'
						+ ' LEFT JOIN area AS a ON c.areaId = a.id'
						+ ' LEFT JOIN location AS l ON a.locationId = l.id'
						+ ' LEFT JOIN county AS co ON a.countyId = co.id';
	*/
	var sql = 'SELECT * FROM client';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function getClient(clientId, callback) {
/*
	var sql = 'SELECT c.id, c.name AS clientName, c.levelOfInterest, c.note, cg.name AS groupName, '
						+ ' i.name AS industry, a.name AS area, l.name AS location, co.name AS county'
						+ ' FROM client AS c'
						+ ' LEFT JOIN clientGroup AS cg ON c.clientGroupId = cg.id'
						+ ' LEFT JOIN industry AS i ON c.industryId = i.id'
						+ ' LEFT JOIN area AS a ON c.areaId = a.id'
						+ ' LEFT JOIN location AS l ON a.locationId = l.id'
						+ ' LEFT JOIN county AS co ON a.countyId = co.id'
						+ ' WHERE id = ?';
*/
	var sql = 'SELECT * FROM client WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[clientId], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function addClient(details, callback) {

	var sql = 'INSERT INTO client (name, levelOfInterest, note, clientGroupId, industryId, locationId) VALUES (?,?,?,?,?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.name, details.levelOfInterest, details.note, details.clientGroupId, details.industryId, details.locationId], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


function deleteClient(clientId, callback) {

	var sql = 'DELETE FROM client WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[clientId], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


function updateClient(details, callback) {

	var sql = 'UPDATE client SET name = ?, note = ?, levelOfInterest = ?, clientGroupId = ?, industryId = ?, locationId = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.name, details.note, details.levelOfInterest, details.clientGroupId, details.industryId, details.locationId, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};



exports.getClients = getClients;
exports.getClient = getClient;
exports.deleteClient = deleteClient;
exports.updateClient = updateClient;
exports.addClient = addClient;
