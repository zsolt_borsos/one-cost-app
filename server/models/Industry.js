
function getIndustries(callback) {

	var sql = 'SELECT * FROM industry';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function getIndustry(id, callback) {

	var sql = 'SELECT * FROM industry WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function addIndustry(details, callback) {

	var sql = 'INSERT INTO industry (name, note) VALUES (?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.name, details.note], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deleteIndustry(id, callback) {

	var sql = 'DELETE FROM industry WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateIndustry(details, callback) {

	var sql = 'UPDATE industry SET name = ?, note = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.name, details.note, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


exports.getIndustry = getIndustry;
exports.getIndustries = getIndustries;
exports.addIndustry = addIndustry;
exports.deleteIndustry = deleteIndustry;
exports.updateIndustry = updateIndustry;
