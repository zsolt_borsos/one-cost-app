
function getProductDetails(callback) {

	var sql = 'SELECT * FROM productDetails';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function getProductDetailByProductId(id, callback){
	var sql = 'SELECT * FROM productDetails WHERE productId = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};


function getProductDetail(id, callback) {

	var sql = 'SELECT * FROM productDetails WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function addProductDetail(details, callback) {

	if (!details.default) {
		details.default = 0;
	}

	var sql = 'INSERT INTO productDetails (description, packsize, productId, `default`) VALUES (?,?,?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.description, details.packsize, details.productId, details.default], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};

function deleteProductDetail(id, callback) {

	var sql = 'DELETE FROM productDetails WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateProductDetail(details, callback) {

	var sql = 'UPDATE productDetails SET description = ?, packsize = ?, productId = ?, `default` = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.description, details.packsize, details.productId, details.default, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

exports.getProductDetailByProductId = getProductDetailByProductId;
exports.getProductDetail = getProductDetail;
exports.getProductDetails = getProductDetails;
exports.addProductDetail = addProductDetail;
exports.deleteProductDetail = deleteProductDetail;
exports.updateProductDetail = updateProductDetail;
