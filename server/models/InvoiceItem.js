
function getInvoiceItems(callback) {

	var sql = 'SELECT * FROM invoiceItem';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function getInvoiceItemByInvoiceId(id, callback) {

	var sql = 'SELECT * FROM invoiceItem WHERE invoiceId = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};



function getInvoiceItemByCc(id, callback) {

	var sql = 'SELECT invoiceItem.* FROM invoiceItem JOIN invoice ON invoice.id = invoiceItem.invoiceId WHERE invoice.ccId = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			console.log('Get invoice by cc db request results:');
			//console.log(results);
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};



function getInvoiceItem(id, callback) {

	var sql = 'SELECT * FROM invoiceItem WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};

function addInvoiceItem(details, callback) {

	var sql = 'INSERT INTO invoiceItem (invoiceId, productId, qty, note) VALUES (?,?,?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.invoiceId, details.productId, details.qty, details.note], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function deleteInvoiceItem(id, callback) {

	var sql = 'DELETE FROM invoiceItem WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function updateInvoiceItem(details, callback) {

	var sql = 'UPDATE invoiceItem SET (invoiceId, productId, qty, note) VALUES (?,?,?,?) WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.invoiceId, details.productId, details.qty, details.note, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};


exports.getInvoiceItemByInvoiceId = getInvoiceItemByInvoiceId;
exports.getInvoiceItemByCc = getInvoiceItemByCc;
exports.getInvoiceItem = getInvoiceItem;
exports.getInvoiceItems = getInvoiceItems;
exports.addInvoiceItem = addInvoiceItem;
exports.deleteInvoiceItem = deleteInvoiceItem;
exports.updateInvoiceItem = updateInvoiceItem;
