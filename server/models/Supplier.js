
function getSuppliers(callback) {

	var sql = 'SELECT * FROM supplier';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});
};


function getSupplier(id, callback) {

	var sql = 'SELECT * FROM supplier WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [id], function(err, results) {
			if (err) {
				callback(err);
			}
			//relese the connection
			connection.release();
			//send the results
			callback(null, results);
		});
	});

};

function addSupplier(details, callback) {

	var sql = 'INSERT INTO supplier (name, note, defaultRebate) VALUES (?,?,?)';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[details.name, details.note, details.defaultRebate], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});

};



function updateSupplier(details, callback) {

	var sql = 'UPDATE supplier SET name = ?, note = ?, defaultRebate = ? WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql, [details.name, details.note, details.defaultRebate, details.id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};

function deleteSupplier(id, callback) {

	var sql = 'DELETE FROM supplier WHERE id = ?';

	global.pool.getConnection(function(err, connection) {
		if (err) {
			callback(err);
		}
		connection.query(sql,[id], function(err, results) {
			if (err) {
				callback(err);
			}
			//release the connection
			connection.release();
			//send the result
			callback(null, results);
		});
	});
};




exports.getSupplier = getSupplier;
exports.getSuppliers = getSuppliers;
exports.addSupplier = addSupplier;
exports.deleteSupplier = deleteSupplier;
exports.updateSupplier = updateSupplier;
