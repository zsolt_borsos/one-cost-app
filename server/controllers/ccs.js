var CC = require('../models/CC');

exports.getCC = function(req, res){
	var id = req.params.id;
	CC.getCC(id, function(err, result){
		//console.log(result);
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			//res.send(err);
			res.sendStatus(404);
		}
	});
};

exports.getCCs = function(req, res){
	CC.getCCs(function(err, result){
		if (!err) {
			res.send(result);
		}else {
			//res.send(err);
			res.sendStatus(404);
		}
	});
};

exports.addCC = function(req, res){
	var details = req.body;

	//get current userId
																			//!!!!!!this needs fixing!!!!!!!!!
																			// using default user in DB.
																			//this could be user group levels later?
	details.userId = 1;

	CC.addCC(details, function(err, result){

		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			//res.send(err);
			res.sendStatus(404);
		}

	});
};

exports.deleteCC = function(req, res) {
	var id = req.params.id;
	CC.deleteCC(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateCC = function(req, res) {
	var details = req.body;
	CC.updateCC(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
