var Product = require('../models/Product');

exports.getProductsBySupplierCount = function(req, res){

	var details = {};
	details.id = req.params.supplierId;

	Product.getProductsBySupplierCount(details, function(err, result){
		if (!err) {
			res.json(result[0]);
		}else {
			res.sendStatus(404);
		}
	});
};


exports.getProductsBySupplier = function(req, res){

	var details = {},
			query = req.query;
	details.id = req.params.supplierId;
	details.page = query.page;
	details.limit = query.limit;
	console.log(req.query);
	Product.getProductsBySupplier(details, function(err, result){
		if (!err) {
			res.json(result);
		}else {
			res.sendStatus(404);
		}
	});
};


exports.getProduct = function(req, res){
	var id = req.params.id;
	Product.getProduct(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getProducts = function(req, res){


		//if id is present in query string then get a single record
		if (req.query.id) {
			var id = req.query.id;
			Product.getProduct(id, function(err, result){
				if (!err) {
					result = result[0];
					res.send(result);
					return;
				}else {
					res.sendStatus(404);
					return;
				}
			});
		}else if (req.query.desc) {
			//console.log('searching by desc!');
			var desc = req.query.desc;
			Product.getProductsByDescription(desc, function(err, result){
				if (!err) {
					res.send(result);
					return;
				}else {
					console.log('Error:');
					console.log(err);
					res.sendStatus(404);
					return;
				}
			});
		}else if (req.query.code) {
			var code = req.query.code;
			Product.getProductsByCode(code, function(err, result){
				if (!err) {
					res.send(result);
					return;
				}else {
					res.sendStatus(404);
					return;
				}
			});
		}else {
			console.log('Doing default on products.');
			Product.getProducts(function(err, result){
				//console.log(err);
				//console.log(result);
				if (!err) {
					res.json(result);
				}else {
					res.sendStatus(404);
				}
			});
		}





};

exports.addProduct = function(req, res){
	var details = req.body;
	Product.addProduct(details, function(err, result){
		console.log(err);
		console.log(result);
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteProduct = function(req, res) {
	var id = req.params.id;
	Product.deleteProduct(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateProduct = function(req, res) {

	var details = req.body;
	Product.updateProduct(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
