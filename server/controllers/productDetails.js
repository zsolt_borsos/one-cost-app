var ProductDetail = require('../models/ProductDetail');

exports.getProductDetail = function(req, res){

	var id = req.params.id;
	ProductDetail.getProductDetail(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getProductDetails = function(req, res){
	//handle productId queries
	if (req.query.productId) {
		var id = req.query.productId;
		ProductDetail.getProductDetailByProductId(id, function(err, result){
			if (!err) {
				result = result[0];
				res.send(result);
			}else {
				res.sendStatus(404);
			}
		});
	}else{
		//default : send all records
		ProductDetail.getProductDetails(function(err, result){
			console.log(err);
			if (!err) {
				res.send(result);
			}else {
				res.sendStatus(404);
			}
		});
	}


};

exports.addProductDetail = function(req, res){
	var details = req.body;
	ProductDetail.addProductDetail(details, function(err, result){
		console.log(err);
		console.log(result);
		console.log(details);
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteProductDetail = function(req, res) {
	var id = req.params.id;
	ProductDetail.deleteProductDetail(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateProductDetail = function(req, res) {
	var details = req.body;
	ProductDetail.updateProductDetail(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
