var Industry = require('../models/Industry');

exports.getIndustry = function(req, res){

	var id = req.params.id;
	Industry.getIndustry(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getIndustries = function(req, res){
	Industry.getIndustries(function(err, result){
		if (!err) {
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.addIndustry = function(req, res){
	var details = req.body;
	Industry.addIndustry(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteIndustry = function(req, res) {
	var id = req.params.id;
	Industry.deleteIndustry(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateIndustry = function(req, res) {
	var details = req.body;
	Industry.updateIndustry(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
