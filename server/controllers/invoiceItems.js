var InvoiceItem = require('../models/InvoiceItem');

exports.getInvoiceItem = function(req, res){

	var id = req.params.id;
	InvoiceItem.getInvoiceItem(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};


exports.getInvoiceItems = function(req, res){
	if (req.query) {
		//console.log('req query:');
		//console.log(req.query);

		//get invoiceItems by invoice (invoiceId)
		if (req.query.invoiceId) {
			var invoiceId = req.query.invoiceId;
			InvoiceItem.getInvoiceItemByInvoiceId(invoiceId, function(err, result){
				if (!err) {
					//console.log('invoiceItems in db:');
					//console.log(result);
					res.send(result);
				}else {
					res.sendStatus(404);
				}
			});
		}

		//get all invoiceItem in a CC
		if (req.query.ccId) {
			console.log('Got ccId. Getting invoiceItems by CC...');
			var ccId = req.query.ccId;
			InvoiceItem.getInvoiceItemByCc(ccId, function(err, result){
				if (!err) {
					//console.log('invoiceItems in db:');
					//console.log(result);
					res.send(result);
				}else {
					res.sendStatus(404);
				}
			});
		}

	}else {
		InvoiceItem.getInvoiceItems(function(err, result){
			if (!err) {
				res.send(result);
			}else {
				res.sendStatus(404);
			}
		});
	}

};

exports.addInvoiceItem = function(req, res){
	var details = req.body;
	InvoiceItem.addInvoiceItem(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteInvoiceItem = function(req, res) {
	var id = req.params.id;
	InvoiceItem.deleteInvoiceItem(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateInvoiceItem = function(req, res) {
	var details = req.body;
	InvoiceItem.updateInvoiceItem(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
