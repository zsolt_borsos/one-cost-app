var ProductMatch = require('../models/ProductMatch');

exports.getProductMatch = function(req, res){

	var id = req.params.id;
	ProductMatch.getProductMatch(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getProductMatches = function(req, res){

	if (req.query.invoiceItemId) {
		var id = req.query.invoiceItemId;
		ProductMatch.getProductMatchByInvoiceItemId(id, function(err, result){
			if (!err) {
				res.send(result);
			}else {
				res.sendStatus(404);
			}
		});
	}else  {
		ProductMatch.getProductMatches(function(err, result){
			if (!err) {
				res.send(result);
			}else {
				res.sendStatus(404);
			}
		});
	}
};

exports.addProductMatch = function(req, res){
	var details = req.body;
	ProductMatch.addProductMatch(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteProductMatch = function(req, res) {
	var id = req.params.id;
	ProductMatch.deleteProductMatch(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateProductMatch = function(req, res) {
	var details = req.body;
	ProductMatch.updateProductMatch(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
