var County = require('../models/County');

exports.getCounty = function(req, res){

	var id = req.params.id;
	County.getCounty(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getCounties = function(req, res){
	County.getCounties(function(err, result){
		if (!err) {
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.addCounty = function(req, res){
	var details = req.body;
	County.addCounty(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteCounty = function(req, res) {
	var id = req.params.id;
	County.deleteCounty(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateCounty = function(req, res) {
	var details = req.body;
	County.updateCounty(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
