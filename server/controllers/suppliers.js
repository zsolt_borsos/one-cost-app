var Supplier = require('../models/Supplier');

exports.getSupplier = function(req, res){

	var id = req.params.id;
	Supplier.getSupplier(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getSuppliers = function(req, res){
	Supplier.getSuppliers(function(err, result){
		if (!err) {
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.addSupplier = function(req, res){
	var details = req.body;
	Supplier.addSupplier(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteSupplier = function(req, res) {
	var id = req.params.id;
	Supplier.deleteSupplier(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateSupplier = function(req, res) {
	var details = req.body;
	Supplier.updateSupplier(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
