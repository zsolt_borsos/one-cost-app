//var fs = require('fs');
var ocimporter = require('oc-importer');

exports.uploadSalesFile = function(req, res) {

	ocimporter.run(req.uploadedFile, function(done){
		console.log('Results', done);
		var path = './' + done.errorFileCreated;
		console.log('path:', path);
		res.download(path);
	});

};
