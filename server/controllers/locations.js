var Location = require('../models/Location');

exports.getLocation = function(req, res){

	var id = req.params.id;
	Location.getLocation(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getLocations = function(req, res){
	
	Location.getLocations(function(err, result){
		if (!err) {
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.addLocation = function(req, res){
	var details = req.body;
	Location.addLocation(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteLocation = function(req, res) {
	var id = req.params.id;
	Location.deleteLocation(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateLocation = function(req, res) {
	var details = req.body;
	Location.updateLocation(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
