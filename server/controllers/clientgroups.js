var ClientGroup = require('../models/ClientGroup');

exports.getClientGroup = function(req, res){

	var id = req.params.id;
	ClientGroup.getClientGroup(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getClientGroups = function(req, res){
	ClientGroup.getClientGroups(function(err, result){
		if (!err) {
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.addClientGroup = function(req, res){
	var details = req.body;
	ClientGroup.addClientGroup(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteClientGroup = function(req, res) {
	var id = req.params.id;
	ClientGroup.deleteClientGroup(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateClientGroup = function(req, res) {
	var details = req.body;
	ClientGroup.updateClientGroup(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
