var Price = require('../models/Price');

exports.getPrice = function(req, res){
	var id = req.params.id;
	Price.getPrice(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getPrices = function(req, res){
	//console.log('got one');
	//check for query string in url

		//get price by invoiceItemId by sourceValue (invoiceItemId)
		if (req.query.invoiceItemId) {
			console.log('Getting product price from invoiceItemId!');
			var invoiceItemId = req.query.invoiceItemId;
			Price.getPriceByInvoiceItemId(invoiceItemId, function(err, result){
				if (!err) {
					//console.log(result);
					result = result[0];
					res.send(result);
					return;
				}else {
					res.sendStatus(404);
					return;
				}
			});
			return;
		}

		//check for individual get requests as well
		if (req.query.id) {
			var id = req.query.id;
			Price.getPrice(id, function(err, result){
				if (!err) {
					result = result[0];
					res.send(result);
					return;
				}else {
					res.sendStatus(404);
					return;
				}
			});
			return;
		}
		//get price(s) by productId
		if (req.query.productId) {
			var id = req.query.productId;
			Price.getPriceByProductId(id, function(err, result){
				if (!err) {
					//result = result[0];
					res.send(result);
					return;
				}else {
					res.sendStatus(404);
					return;
				}
			});
			return;
		}

		if (req.query.typeId) {
			//console.log('typeid !!!!!!!!');
			var id = req.query.typeId;
			Price.getPriceByType(id, function(err, result){
				if (!err) {
					//result = result[0];
					res.send(result);
					return;
				}else {
					res.sendStatus(404);
					return;
				}
			});
			return;
		}



		console.log('Doing default, no parameter was spotter.');
		//default: send back all records
		Price.getPrices(function(err, result){
			if (!err) {
				res.send(result);
			}else {
				res.sendStatus(404);
			}
		});

};

exports.addPrice = function(req, res){
	var details = req.body;
	Price.addPrice(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deletePrice = function(req, res) {
	var id = req.params.id;
	Price.deletePrice(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updatePrice = function(req, res) {
	var details = req.body;
	Price.updatePrice(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
