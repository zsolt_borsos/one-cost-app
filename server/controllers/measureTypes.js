var MeasureType = require('../models/MeasureType');

exports.getMeasureType = function(req, res){

	var id = req.params.id;
	MeasureType.getMeasureType(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getMeasureTypes = function(req, res){
	MeasureType.getMeasureTypes(function(err, result){
		if (!err) {
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.addMeasureType = function(req, res){
	var details = req.body;
	MeasureType.addMeasureType(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteMeasureType = function(req, res) {
	var id = req.params.id;
	MeasureType.deleteMeasureType(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateMeasureType = function(req, res) {
	var details = req.body;
	MeasureType.updateMeasureType(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
