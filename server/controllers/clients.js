var Client = require('../models/Client');

exports.getClient = function(req, res){

	var id = req.params.id;
	Client.getClient(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.getClients = function(req, res){
	Client.getClients(function(err, result){
		//console.log(err);
		//console.log(result);
		if (!err) {
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.addClient = function(req, res){
	var details = req.body;
	Client.addClient(details, function(err, result){
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteClient = function(req, res) {
	var id = req.params.id;
	Client.deleteClient(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};

exports.updateClient = function(req, res) {
	var details = req.body;
	Client.updateClient(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(details);
			}
		}else {
			res.status(409).send(err.code);
		}
	});
};
