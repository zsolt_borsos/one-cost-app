var Invoice = require('../models/Invoice');
var fs = require('fs');

exports.getInvoiceImage = function(req, res){

	var id = req.params.id;
	Invoice.getInvoice(id, function(err, result){
		if (!err) {
			result = result[0];
			var file = './uploads/' + result.ccId + '/' + result.imageName;
				fs.stat(file, function (err, stat) {
					if (err) {
						console.log('Failed to open invoice image file.');
						res.sendStatus(409);
					}else{
						var img = fs.readFileSync(file);
						res.contentType = 'image/png';
						res.contentLength = stat.size;
						res.end(img, 'binary');
					}
				});
			}else {
			res.sendStatus(404);
		}
	});
};




exports.getInvoice = function(req, res){

	var id = req.params.id;
	Invoice.getInvoice(id, function(err, result){
		if (!err) {
			result = result[0];
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};


exports.getInvoices = function(req, res){
	Invoice.getInvoices(function(err, result){
		if (!err) {
			res.send(result);
		}else {
			res.sendStatus(404);
		}
	});
};

exports.addInvoice = function(req, res){

	var details = req.body;
	var files = req.files;
	var fileName = files.file[0].name;
	console.log(fileName);
	details.imageName = fileName;

	if (!details.ccId) {
		console.log('Missing ccId');
		res.sendStatus(404);
	}

	Invoice.addInvoice(details, function(err, result){
		console.log('err', err);
		console.log('result', result);
		if (!err) {
			details.id = result.insertId;
			res.send(details);
		}else {
			res.sendStatus(404);
		}

	});
};

exports.deleteInvoice = function(req, res) {
	console.log(req.body);

	var id = req.params.id;
	Invoice.deleteInvoice(id, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				res.send(id);
			}
		}else {
			res.sendStatus(409).send(err.code);
		}
	});
};

exports.updateInvoice = function(req, res) {
	var details = req.body;
	Invoice.updateInvoice(details, function(err, result){
		console.log(err);
		if (!err) {
			if (result) {
				//console.log('result:', result);
				//console.log('body:', req.body);
				res.send(details);
			}
		}else {
			res.sendStatus(409).send(err.code);
		}
	});
};
