var request = require('supertest'),
		app = require('./../../server');


describe('Request to the root path', function() {

	it('Returns HTML format', function(done) {

		request(app)
			.get('/')
			.expect('Content-Type', /html/, done)
	});

	it('Returns 200 status code', function(done) {

		request(app)
			.get('/')
			.expect(200, done)
	});


});

describe('Requests to api/v1/client resource', function() {

	it('Returns 200 status code', function(done){
		request(app)
			.get('/api/v1/clients')
			.expect(200, done)
	});

	it('Returns JSON format', function(done){
		request(app)
			.get('/api/v1/clients')
			.expect('Content-Type', /json/, done)
	});

	it('Returns a client in JSON format', function(done){
		request(app)
			.get('/api/v1/clients/1')
			.expect(200, done)
	});

});

describe('Requests to api/v1/cc', function() {

	it('Returns 200 status code', function(done){
		request(app)
			.get('/api/v1/ccs')
			.expect(200, done)
	});

	it('Returns JSON format', function(done){
		request(app)
			.get('/api/v1/ccs')
			.expect('Content-Type', /json/, done)
	});

	it('Returns a cc in JSON format', function(done){
		request(app)
			.get('/api/v1/ccs/1')
			.expect(200, done)
	});

});
