angular.module('app', ['ngResource', 'ngRoute', 'angularFileUpload', 'socket-io', 'ui.bootstrap', 'angularSpinner', 'mp.datePicker', 'chart.js']);


angular.module('app').config(function($routeProvider, $locationProvider){

    var routeRoleChecks = {

        admin: { auth: function(mvAuth) {
            return mvAuth.authoriseCurrentUserForRoute('admin')
        }},
        user: { auth: function(mvAuth) {
            return mvAuth.authorisedAuthenticatedUserForRoute()
        }}

    }

    $locationProvider.html5Mode(true);


    $routeProvider
        .when('/', { templateUrl: '/partials/main/main', controller: 'mvMainCtrl' })

        .when('/admin/users', { templateUrl: '/partials/admin/user-list',
                                controller: 'mvUserListCtrl',
                                resolve: routeRoleChecks.admin
                              })

        .when('/signup', {  templateUrl: '/partials/account/signup',
                            controller: 'mvSignupCtrl'
                          })
        .when('/profile', { templateUrl: '/partials/account/profile',
                            controller: 'mvProfileCtrl',
                            resolve:routeRoleChecks.user
                          })

                              //these need login check later!!!!!!!!!!!!!

                              //clients
        .when('/clients', { templateUrl: '/partials/client/client-list',
                            controller: 'mvClientListCtrl'
                          })
        .when('/clients/:id', { redirectTo: '/clients/:id/clientmanager' })
        .when('/clients/:id/clientmanager', { templateUrl: '/partials/clientManager/client-manager',
                                    controller: 'mvClientManagerCtrl'
                                  })

                              //client groups
        .when('/clientgroups', {  templateUrl: '/partials/clientgroup/clientgroup-list',
                                  controller: 'mvClientGroupListCtrl'
                                })
                              //industry
        .when('/industries', {  templateUrl: '/partials/industry/industry-list',
                              controller: 'mvIndustryListCtrl'
                            })
                                  //location
        .when('/locations', {  templateUrl: '/partials/location/location-list',
                              controller: 'mvLocationListCtrl'
                            })
                                  //county
        .when('/counties', {  templateUrl: '/partials/county/county-list',
                            controller: 'mvCountyListCtrl'
                          })
        .when('/ccs', {  templateUrl: '/partials/ccs/cc-list',
                            controller: 'mvCCListCtrl'
                          })
        .when('/suppliers', {  templateUrl: '/partials/supplier/supplier-list',
                            controller: 'mvSupplierListCtrl'
                          })
        .when('/suppliers/:supplierId/products', {  templateUrl: '/partials/product/product-list',
                            controller: 'mvProductListCtrl'
                          })
        .when('/ccs/:id', {  redirectTo: '/ccs/:id/ccmanager' })
        .when('/ccs/:id/ccmanager', {  templateUrl: '/partials/ccs/cc-manager',
                            controller: 'mvCCManagerCtrl'
                          })
        .when('/ccs/:ccId/ccmanager/invoices/:invoiceId', { redirectTo: '/ccs/:ccId/ccmanager/invoices/:invoiceId/invoicemanager' })
        .when('/ccs/:ccId/ccmanager/invoices/:invoiceId/invoicemanager', {  templateUrl: '/partials/invoiceManager/invoiceManager',
                            controller: 'mvInvoiceManagerCtrl'
                          })

        .when('/products', {  templateUrl: '/partials/product/product-list',
                            controller: 'mvProductListCtrl'
                          })
        .when('/ccs/:ccId/productmatcher', {  templateUrl: '/partials/productMatcher/productMatcher',
                            controller: 'mvProductMatcherCtrl'
                          })
        .when('/compareProducts', {  templateUrl: '/partials/productComparison/compare-products',
                            controller: 'mvProductComparisonCtrl'
                          })
        .when('/sales', {  templateUrl: '/partials/sale/sales-tracker',
                            controller: 'mvSalesTrackerCtrl'
                          })
        .when('/import', {  templateUrl: '/partials/import/import-main',
                            controller: 'mvImportMainCrtl'
                          })


});

angular.module('app').run(function($rootScope, $location) {


    $rootScope.$on('$routeChangeError', function(evt, current, previous, rejection) {

        if (rejection === 'not authorised') {
            $location.path('/');
        }

    })

});
