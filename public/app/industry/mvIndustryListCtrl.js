angular.module('app').controller('mvIndustryListCtrl', function($scope, mvNotifier, mvIndustry, socket) {

	$scope.records = mvIndustry.query();
	$scope.searchText = '';
	$scope.adding = false;
	$scope.recordCopy = undefined;
	$scope.currentRecord = undefined;


	$scope.edit = function(record) {
		if ($scope.recordCopy != undefined) {
			$scope.cancel($scope.currentRecord);
			$scope.currentRecord.editing = false;
		}
		//console.log('Editing record');
		record.editing = true;
		$scope.currentRecord = record;
		$scope.recordCopy = angular.copy(record);

	};


	$scope.update = function(record) {
		mvIndustry.update({id: record.id}, record);
		mvNotifier.notify('Industry details updated.');
		$scope.recordCopy = undefined;
		record.editing = false;
		socket.emit('updateIndustries');
	};

	$scope.add = function() {
		$scope.adding = true;
	};


	$scope.cancel = function(record) {
		var id = $scope.records.indexOf(record);
		$scope.records[id] = angular.copy($scope.recordCopy);
		$scope.records[id].editing = false;
		$scope.recordCopy = undefined;
	};

	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var deleted = record.$delete();
			deleted.then(function(success){
				mvNotifier.notify('Record deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
				socket.emit('updateIndustries');
			}, function(error){
				mvNotifier.error('Failed to delete record...'+ error.statusText +' : '+ error.data);
			});
		}
	};

	$scope.$on('finishedAdding', function(event, args) {
    $scope.adding = false;
		if (args == true) {
			$scope.records = mvIndustry.query();
			socket.emit('updateIndustries');
		}
  });

	socket.on('updateIndustries', function() {
		console.log('Providing live data! Woohoo!!');
		$scope.records = mvIndustry.query();
	});


});
