angular.module('app').controller('mvIndustryAddCtrl', function($scope, mvNotifier, mvIndustry) {

	$scope.record = new mvIndustry;
	$scope.name = '';
	$scope.note = '';

	$scope.create = function() {
		if ($scope.name.length > 0) {
			$scope.record.name = $scope.name;
			$scope.record.note = $scope.note;
			var added = $scope.record.$save();
			added.then(function(success){
				$scope.$emit('finishedAdding', true);
				mvNotifier.notify('Industry added successfully.');
			}, function(error){
				mvNotifier.error('Failed to add record.');
			});
		}
	};

	$scope.cancel = function() {
		$scope.$emit('finishedAdding', false);
	};

});
