angular.module('app').factory('mvIndustry', function($resource) {

  var IndustryResource = $resource('/api/v1/industries/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return IndustryResource;

});
