angular.module('app').factory('mvIndustryService', function(mvIndustry) {

	var industries = mvIndustry.query();

	return {

		refresh : function() {
			industries = mvIndustry.query();
			return industries;
		},

		getAll : function() {
			return industries;
		},

		getIndustryName : function(industryId) {
	    if (industryId) {
	      for (var id in industries){
	        //console.log(industries[id]);
	        if (industries[id].id === industryId) {
	          return industries[id].name;
	        }
	      }
	    }
	  },

		getIndustryId : function(industryId) {
	    if (industryId) {
	      for (var id in industries){
	        //console.log(industries[id]);
	        if (industries[id].id === industryId) {
	          return id;
	        }
	      }
	    }
	  }

	}

});
