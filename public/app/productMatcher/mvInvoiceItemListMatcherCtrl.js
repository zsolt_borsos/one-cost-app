angular.module('app').controller('mvInvoiceItemListMatcherCtrl', function($scope, $routeParams, socket, $timeout, $q, mvNotifier, mvInvoiceItem, mvProductDetailService, mvProduct, mvPrice, mvSpinner) {

	$scope.searchText = '';
	$scope.records = [];

	$scope.constructme = function() {
		//socket.emit('spinnerStart');
			var items = mvInvoiceItem.query({ccId: $routeParams.ccId});
			//console.log(items);
			items.$promise.then(function(data){
			$scope.records = data;
			//console.log(data);

			$scope.records.$promise.then(function(){
				//$scope.currentInvoiceTotal = 0;
				for (var i = 0; i < $scope.records.length; i++) {
					var record = $scope.records[i];
					console.log(record);
					var product = mvProduct.get({id: record.productId});
					record.description = mvProductDetailService.pgetDesc(record.productId);
					record.packsize = mvProductDetailService.pgetPacksize(record.productId);
					record.product = product;
					//console.log(product);
					record.price = mvPrice.get({invoiceItemId: record.id});
					$scope.records[i] = record;
					if (i === $scope.records.length) {

						return $scope.records;
					}
					//console.log(record);
				}
			});

		}).then(function(data){
			socket.emit('spinnerStop');
		});

	};

//need to delay "construction" so services can be loaded 1st
socket.emit('spinnerStart');
$timeout(function(){
	$scope.constructme();
}, 500);


	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var deleted = record.$delete();
			deleted.then(function(success) {
				mvNotifier.notify('Record deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
			}, function(error){
				mvNotifier.error('Failed to delete record...'+ error.statusText +' : '+ error.data);
			});
		}
	};

	$scope.selectItem = function(item) {
		$scope.selectedItem = item;
		$scope.$emit('selectedItemToMatch', item);
	};

	$scope.selected = function(item){
		if (item === $scope.selectedItem) {
			return true;
		}else {
			return false;
		}
	}

	socket.on('updateInvoiceItems', function(){
		socket.emit('spinnerStart');
		mvNotifier.notify('Updating invoice items list for CC views...');
		//$scope.products = mvProductService.refresh();
		//$scope.products.$promise.then(function(){
			$scope.details = mvProductDetailService.refresh();
			$scope.details.$promise.then(function(){
				$scope.constructme();
				//socket.emit('spinnerStop');
			});
		//});
	});

});
