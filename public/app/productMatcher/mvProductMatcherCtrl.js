angular.module('app').controller('mvProductMatcherCtrl', function($scope, socket, $routeParams, $timeout, mvNotifier, mvProduct, filterFilter, mvSpinner) {

	//defaults
	$scope.ccId = $routeParams.ccId;
	$scope.searchText = '';
	$scope.currentProduct = undefined;

	$scope.$on('selectedItemToMatch', function(event, args) {
		mvNotifier.notify('Product selected to match...');
		$scope.currentProduct = args;
		console.log('currentProduct', $scope.currentProduct);
		$scope.$broadcast('selectedProductToMatch', $scope.currentProduct);
		
	});



});
