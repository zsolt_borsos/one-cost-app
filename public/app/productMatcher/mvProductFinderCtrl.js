angular.module('app').controller('mvProductFinderCtrl', function($scope, mvNotifier, mvProduct, socket) {

	$scope.searchBy = 'desc';
	$scope.searchDbText = '';

	$scope.findProduct = function() {
		socket.emit('spinnerStart');
		var searchTxt = $scope.searchDbText;
		if ($scope.searchBy === 'desc') {
			$scope.products = mvProduct.query({ 'desc': searchTxt}, function(){
				socket.emit('spinnerStop');
			});
		}else {
			$scope.products = mvProduct.query({ 'code': searchTxt}, function(){
				socket.emit('spinnerStop');
			});
		}

	};


	$scope.addMatch = function() {
		
	};

});
