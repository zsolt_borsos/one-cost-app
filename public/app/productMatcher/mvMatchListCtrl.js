angular.module('app').controller('mvMatchListCtrl', function($scope, mvNotifier, socket, mvPrice, mvProduct, mvProductDetail, mvProductMatch) {

	$scope.records = [];

	$scope.getMatchedItems = function() {
		$scope.records = mvProductMatch.query({invoiceItemId : $scope.currentProduct.id }, function(result){
			for (var i = 0; i < result.length; i++) {
				result[i].product = mvProduct.get({id: result[i].productId});
				result[i].price = mvPrice.get({id: result[i].priceId});
				result[i].details = mvProductDetail.get({productId: result[i].productId});
			}
		});
	};

	$scope.$on('selectedProductToMatch', function(event, args) {
		$scope.currentProduct = args;
		$scope.getMatchedItems();
	});


	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var deleted = record.$delete();
			deleted.then(function(success) {
				mvNotifier.notify('Record deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
			}, function(error){
				mvNotifier.error('Failed to delete record...'+ error.statusText +' : '+ error.data);
			});
		}
	};


});
