angular.module('app').factory('mvProductMatch', function($resource) {

  var ProductMatchResource = $resource('/api/v1/productMatch/:id', {}, {
    update: { method: 'PUT', isArray: false }
  });

  return ProductMatchResource;

});
