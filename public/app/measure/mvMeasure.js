angular.module('app').factory('mvMeasure', function($resource) {

  var MeasureResource = $resource('/api/v1/measures/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return MeasureResource;

});
