angular.module('app').value('mvToastr', toastr);

angular.module('app').factory('mvNotifier', function(mvToastr, socket) {

  mvToastr.options.closeButton = true;

    socket.on('globalNotify', function(msg){
        mvToastr.success(msg);
        console.log('Global msg:' + msg);
    });

    socket.on('globalError', function(msg){
        mvToastr.error(msg);
        console.log('Global error msg:' + msg);
    });

    return {
        notify: function (msg) {
            mvToastr.success(msg);
            console.log(msg);
        },
        error: function(msg) {
            mvToastr.error(msg);
            console.log(msg);
        },
        globalNotify: function(msg){
          mvToastr.success(msg);
          socket.emit('globalNotify', msg);
          console.log(msg);
        },
        globalError: function(msg){
          mvToastr.error(msg);
          socket.emit('globalError', msg);
          console.log(msg);
        }

    }
});
