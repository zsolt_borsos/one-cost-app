angular.module('app').factory('mvSpinner', function(usSpinnerService, socket) {

	socket.on('spinnerStart', function() {
		usSpinnerService.spin('spinner');
	});

	socket.on('spinnerStop', function() {
		usSpinnerService.stop('spinner');
	});


	return {
		startSpin: function(){
			usSpinnerService.spin('spinner');
		},
		stopSpin: function(){
			usSpinnerService.stop('spinner');
		}

	};



});
