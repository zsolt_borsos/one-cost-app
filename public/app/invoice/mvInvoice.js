angular.module('app').factory('mvInvoice', function($resource) {

  var InvoiceResource = $resource('/api/v1/invoices/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return InvoiceResource;

});
