angular.module('app').controller('mvInvoiceAddCtrl', function($scope, $upload, mvNotifier, mvInvoice) {


	if ($scope.$parent.ccId) {
		$scope.ccId = $scope.$parent.ccId;
		console.log('ccId: ' + $scope.ccId);
	}

	$scope.showDatePicker = false;

	$scope.resetState = function() {
		$scope.files = [];
		$scope.date = undefined;
		$scope.note = '';
	};

	$scope.resetState();

	$scope.toggleDatePicker = function() {
		$scope.showDatePicker = !$scope.showDatePicker;
	};

	$scope.dateEntered = function() {
		$scope.showDatePicker = false;
	};

  $scope.upload = function (files) {
      if (files && files.length) {
          for (var i = 0; i < files.length; i++) {
              var file = files[i];
              $upload.upload({
                  url: '/api/v1/invoices',
                  fields: {
										'ccId': $scope.ccId,
										'date': $scope.date,
										'note': $scope.note
									},
                  file: file
              }).progress(function (evt) {
                  var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                  console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
              }).success(function (data, status, headers, config) {
                  console.log('file ' + config.file.name + 'uploaded. Response: ' + data);

									//notifying parent scopes so it can react on the changes -> show just added record when file is uploaded and posted?
									$scope.$emit('invoiceAdded', true);
									mvNotifier.notify('Invoice added successfully.');
              });
          }
      }
  };


	$scope.create = function() {

		if ($scope.files.length > 0) {
			$scope.record = new mvInvoice;

			if ($scope.ccId) {
				$scope.record.ccId = $scope.ccId;
			}else {
				console.log('No ccId!');
			}

			if ($scope.date) {
				$scope.record.date = $scope.date;
			}else {
				$scope.record.date = '0000-00-00';

			}
			if ($scope.note) {
				$scope.record.note = $scope.note;
			}else {
				$scope.record.note = null;
			}
			//start uploading the file
			$scope.upload($scope.files);

			$scope.resetState();
			/*
			var added = $scope.record.$save();
			added.then(function(success){
				//$scope.$emit('finishedAdding', true);
				mvNotifier.notify('Invoice added successfully.');
			}, function(error){
				console.log(error);
				mvNotifier.error('Failed to add record.');
			});
			*/

		}else {
			mvNotifier.error('Select an image first...');
		}

	};


});
