angular.module('app').factory('mvInvoiceService', function(mvInvoice) {

	//using new style for services
	var invoices = mvInvoice.query();

	return {

		refresh : function() {
			invoices = mvInvoice.query();
			return invoices;
		},

		getAll : function() {
			return invoices;
		},

		getName : function(invoiceId) {
	    if (invoiceId) {
	      for (var id in invoices){
	        //console.log(invoices[id]);
	        if (invoices[id].id === invoiceId) {
	          return invoices[id].name;
	        }
	      }
	    }
	  },

		getId : function(invoiceId) {
	    if (invoiceId) {
	      for (var id in invoices){
	        //console.log(invoices[id]);
	        if (invoices[id].id === invoiceId) {
	          return id;
	        }
	      }
	    }
	  }

	}

});
