angular.module('app').controller('mvInvoiceListCtrl', function($scope, $timeout, socket, mvNotifier, mvInvoice, mvSupplierService) {

	$scope.records = mvInvoice.query();
	$scope.searchText = '';
	$scope.adding = false;
	$scope.recordCopy = undefined;
	$scope.currentRecord = undefined;

	$scope.getSupplierName = function(id){
		return mvSupplierService.getName(id);
	};

	$scope.edit = function(record) {
		if ($scope.recordCopy != undefined) {
			$scope.cancel($scope.currentRecord);
			$scope.currentRecord.editing = false;
		}

		//console.log('Editing record');
		record.editing = true;
		$scope.currentRecord = record;
		$scope.recordCopy = angular.copy(record);

	};

	$scope.update = function(record) {
		record.ccId = $scope.$parent.ccId;
		mvInvoice.update({id: record.id}, record);
		mvNotifier.notify('Invoice details updated.');
		$scope.recordCopy = undefined;
		record.editing = false;
		socket.emit('updateInvoices');
	};

	$scope.cancel = function(record) {
		var id = $scope.records.indexOf(record);
		$scope.records[id] = angular.copy($scope.recordCopy);
		$scope.records[id].editing = false;
		$scope.recordCopy = undefined;
	};

	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var deleted = record.$delete();
			deleted.then(function(success){
				mvNotifier.notify('Record deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
				socket.emit('updateInvoices');
			}, function(error){
				mvNotifier.error('Failed to delete record...'+ error.statusText +' : '+ error.data);
			});
		}
	};

	$scope.$on('updateInvoices', function(event, args) {
		console.log('updating invoices view with events!');
		socket.emit('updateInvoices');
		$timeout(function(){
			$scope.records = mvInvoice.query();
		});
  });

	socket.on('updateInvoices', function() {
		console.log('Providing live data! Woohoo!!');
		//mvNotifier.notify('New Invoice added.');
    $scope.records = mvInvoice.query();
  });

});
