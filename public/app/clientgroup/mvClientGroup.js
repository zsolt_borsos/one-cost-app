angular.module('app').factory('mvClientGroup', function($resource) {

  var ClientGroupResource = $resource('/api/v1/clientgroups/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return ClientGroupResource;

});
