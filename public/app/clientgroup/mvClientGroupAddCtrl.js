angular.module('app').controller('mvClientGroupAddCtrl', function($scope, mvNotifier, mvClientGroup) {

	$scope.record = new mvClientGroup;
	$scope.name = '';
	$scope.note = '';

	$scope.create = function() {
		if ($scope.name.length > 0) {
			$scope.record.name = $scope.name;
			$scope.record.note = $scope.note;
			var added = $scope.record.$save();
			added.then(function(success){
				$scope.$emit('finishedAdding', true);
				mvNotifier.notify('Group added successfully.');
			}, function(error){
				mvNotifier.error('Failed to add group.');
			});
		}
	};


	$scope.cancel = function() {
		$scope.$emit('finishedAdding', false);
	};

});
