angular.module('app').factory('mvClientGroupService', function(mvClientGroup) {

	var clientGroups = mvClientGroup.query();

	return {

		refresh : function() {
			clientGroups = mvClientGroup.query();
			return clientGroups;
		},

		getAll : function() {
			return clientGroups;
		},

		getClientGroupName : function(clientGroupId) {
	    if (clientGroupId) {
	      for (var id in clientGroups){
	        //console.log(clientGroups[id]);
	        if (clientGroups[id].id === clientGroupId) {
	          return clientGroups[id].name;
	        }
	      }
	    }
	  },

		getClientGroupId : function(clientGroupId) {
	    if (clientGroupId) {
	      for (var id in clientGroups){
	        //console.log(clientGroups[id]);
	        if (clientGroups[id].id === clientGroupId) {
	          return id;
	        }
	      }
	    }
	  }

	}

});
