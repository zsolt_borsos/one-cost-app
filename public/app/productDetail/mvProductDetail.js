angular.module('app').factory('mvProductDetail', function($resource) {

  var ProductDetailResource = $resource('/api/v1/productDetails/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return ProductDetailResource;

});
