angular.module('app').factory('mvProductDetailService', function(mvProductDetail, $q) {

	var productDetails = mvProductDetail.query();

	return {

		refresh : function() {
			return productDetails = mvProductDetail.query();
		},

		getAll : function() {
			return productDetails;
		},

		//get stuff by productDetailId
		getId : function(productDetailId) {
			if (productDetailId) {
				for (var id in productDetails){
					if (productDetails[id].id === productDetailId) {
						return id;
					}
				}
			}
		},

		getDesc : function(productDetailId) {
	    if (productDetailId) {
	      for (var id in productDetails){
	        if (productDetails[id].id === productDetailId) {
	          return productDetails[id].description;
	        }
	      }
	    }
	  },

		getPacksize : function(productDetailId) {
			if (productDetailId) {
				for (var id in productDetails){
					//console.log(productDetails[id]);
					if (productDetails[id].id === productDetailId) {
						return productDetails[id].packsize;
					}
				}
			}
		},

		//get stuff by productId
		pgetId : function(productId) {
			if (productId) {
				for (var id in productDetails){
					if (productDetails[id].productId === productId) {
						return id;
					}
				}
			}
		},

		pgetDesc : function(productId) {
	    if (productId) {
	      for (var id in productDetails){
	        if (productDetails[id].productId === productId) {
	          return productDetails[id].description;
	        }
	      }
	    }
	  },

		pgetPacksize : function(productId) {
			if (productId) {
				for (var id in productDetails){
					if (productDetails[id].productId === productId) {
						return productDetails[id].packsize;
					}
				}
			}
		}

	}

});
