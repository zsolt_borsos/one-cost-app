angular.module('app').factory('mvCounty', function($resource) {

  var CountyResource = $resource('/api/v1/counties/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return CountyResource;

});
