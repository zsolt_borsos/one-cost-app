angular.module('app').controller('mvCountyAddCtrl', function($scope, mvNotifier, mvCounty) {

	$scope.record = new mvCounty;
	$scope.name = '';

	$scope.create = function() {
		if ($scope.name.length > 0) {
			$scope.record.name = $scope.name;
			var added = $scope.record.$save();
			added.then(function(success){
				$scope.$emit('finishedAdding', true);
				mvNotifier.notify('County added successfully.');
			}, function(error){
				mvNotifier.error('Failed to add record.');
			});
		}
	};

	$scope.cancel = function() {
		$scope.$emit('finishedAdding', false);
	};

});
