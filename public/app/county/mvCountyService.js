angular.module('app').factory('mvCountyService', function(mvCounty) {

	var counties = mvCounty.query();

	return {

		refresh : function() {
			counties = mvCounty.query();
			return counties;
		},

		getAll : function() {
			return counties;
		},

		getCountyName : function(countyId) {
	    if (countyId) {
	      for (var id in counties){
	        //console.log(counties[id]);
	        if (counties[id].id === countyId) {
	          return counties[id].name;
	        }
	      }
	    }else {
	      return "Not specified.";
	    }
	  },

		getCountyId : function(countyId) {
	    if (countyId) {
	      for (var id in counties){
	        //console.log(counties[id]);
	        if (counties[id].id === countyId) {
	          return id;
	        }
	      }
	    }
	  }

	}



});
