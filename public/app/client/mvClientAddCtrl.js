angular.module('app').controller('mvClientAddCtrl', function($scope, mvNotifier, mvClient, mvIndustryService, mvLocationService, mvClientGroupService) {

	$scope.record = new mvClient;
	$scope.name = '';
	$scope.levelOfInterest = 1;
	$scope.note = '';
	$scope.groups = mvClientGroupService.getAll();
	$scope.industries = mvIndustryService.getAll();
	$scope.locations = mvLocationService.getAll();

	$scope.createRecord = function() {
		if ($scope.name.length > 0) {
			$scope.record.name = $scope.name;
			$scope.record.levelOfInterest = $scope.levelOfInterest;
			$scope.record.note = $scope.note;
			if ($scope.selectedGroup) {
				$scope.record.clientGroupId = $scope.selectedGroup.id;
			}
			if ($scope.selectedIndustry) {
				$scope.record.industryId = $scope.selectedIndustry.id;
			}
			if ($scope.selectedLocaton) {
				$scope.record.locationId = $scope.selectedLocaton.id;
			}

			var added = $scope.record.$save();
			added.then(function(success){
				$scope.$emit('finishedAdding', true);
				mvNotifier.notify('Client added successfully.');
			}, function(error){
				mvNotifier.error('Failed to add client.');
			});
		}else {
			mvNotifier.error('Name cannot be empty...');
		}
	};


	$scope.cancel = function() {
		$scope.$emit('finishedAdding', false);
	};

});
