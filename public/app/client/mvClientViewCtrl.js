angular.module('app').controller('mvClientViewCtrl', function($scope, mvNotifier, mvClient, mvClientService, mvIndustryService, mvLocationService, mvClientGroupService, $routeParams) {

	var recordId = $routeParams.id;
	console.log(recordId);

	if ($scope.$parent.currentRecord) {
		console.log($scope.$parent.currentRecord);
		$scope.currentRecord = $scope.$parent.currentRecord;
	}else {

		var currentRecord = mvClient.get({id:recordId});
		console.log(currentRecord);
			$scope.currentRecord = currentRecord;
	}

	$scope.currentRecord.groupName = mvClientGroupService.getClientGroupName($scope.currentRecord.clientGtoupId);
	$scope.currentRecord.industry = mvIndustryService.getIndustryName($scope.currentRecord.industryId);
	$scope.currentRecord.locationName = mvLocationService.getLocationName($scope.currentRecord.locationId);

});
