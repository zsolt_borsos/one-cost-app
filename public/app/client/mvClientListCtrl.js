angular.module('app').controller('mvClientListCtrl', function($scope, socket, mvNotifier, mvClient, mvIndustry, mvLocation, mvClientGroup) {

	$scope.groups = mvClientGroup.query();
	$scope.industries = mvIndustry.query();
	$scope.locations = mvLocation.query();

	$scope.records = mvClient.query(function() {

		$scope.records.forEach(function(record){
			//console.log('record:', record);
			if (record.clientGroupId) {
				record.selectedGroup = mvClientGroup.get({id: record.clientGroupId});
			}
			if (record.industryId) {
				record.selectedIndustry = mvIndustry.get({id: record.industryId});
			}
			if (record.locationId) {
				record.selectedLocation = mvLocation.get({id: record.locationId});
			}
			//return record;
		});

	});
	$scope.searchText = '';
	$scope.adding = false;
	$scope.recordCopy = undefined;
	$scope.currentRecord = undefined;


	$scope.edit = function(record) {
		if ($scope.recordCopy != undefined) {
			$scope.cancel($scope.currentRecord);
			$scope.currentRecord.editing = false;
		}
		//console.log('Editing user');
		console.log('Client Record:', record);
		record.editing = true;
		$scope.currentRecord = record;
		$scope.recordCopy = angular.copy(record);
	};

	$scope.update = function(record) {
		if (record.selectedIndustry) {
			record.industryId = record.selectedIndustry.id;
		}
		if (record.selectedLocation) {
			record.locationId = record.selectedLocation.id;
		}
		if (record.selectedGroup) {
			record.clientGroupId = record.selectedGroup.id;
		}
		mvClient.update({id: record.id}, record);
		mvNotifier.notify('Client details updated.');
		$scope.recordCopy = undefined;
		record.editing = false;
		socket.emit('updateClients');

	};

	$scope.add = function() {
		$scope.adding = true;
	};


	$scope.cancel = function(record) {
		var id = $scope.records.indexOf(record);
		$scope.records[id] = angular.copy($scope.recordCopy);
		$scope.records[id].editing = false;
		$scope.recordCopy = undefined;
	};

	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var recordName = record.name;
			var deleted = record.$delete();
			deleted.then(function(success){
				mvNotifier.notify('record: ' + recordName + ' deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
				socket.emit('updateClients');
			}, function(error){
				mvNotifier.error('Failed to delete record...'+ error.statusText +' : '+ error.data);
			});
		}
	};


	$scope.$on('finishedAdding', function(event, args) {
    $scope.adding = false;
		if (args == true) {
			$scope.records = mvClient.query();
			socket.emit('updateClients');
		}
  });

	socket.on('updateClients', function() {
		console.log('Providing live data! Woohoo!!');
		$scope.records = mvClient.query();
	});

	socket.on('updateGroups', function() {
		console.log('Providing live data! Woohoo!!');
		$scope.groups = mvClientGroup.query();
	});
	socket.on('updateIndustries', function() {
		console.log('Providing live data! Woohoo!!');
		$scope.industries = mvIndustry.query();
	});
	socket.on('updateLocations', function() {
		console.log('Providing live data! Woohoo!!');
		$scope.locations = mvLocation.query();
	});


});
