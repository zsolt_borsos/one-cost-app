angular.module('app').factory('mvClient', function($resource) {

  var ClientResource = $resource('/api/v1/clients/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return ClientResource;

});
