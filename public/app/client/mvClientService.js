angular.module('app').factory('mvClientService', function(mvClient) {

	var clients = mvClient.query();

	return {

		refresh : function() {
			clients = mvClient.query();
			return clients;
		},

		getAll : function() {
			return clients;
		},

		getName : function(clientId) {
	    if (clientId) {
	      for (var id in clients){
	        //console.log(clients[id]);
	        if (clients[id].id === clientId) {
	          return clients[id].name;
	        }
	      }
	    }else {
	      return "Not specified.";
	    }
	  },

		getId : function(clientId) {
	    if (clientId) {
	      for (var id in clients){
	        //console.log(clients[id]);
	        if (clients[id].id === clientId) {
	          return id;
	        }
	      }
	    }
	  }

	}



});
