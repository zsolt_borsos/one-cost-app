angular.module('app').controller('mvUserListCtrl', function($scope, mvUser, mvNotifier) {

    $scope.users = mvUser.query();
    $scope.editing = false;
    $scope.defaultPass = '12345';
    $scope.fname = '';
    $scope.lname = '';
    $scope.uname = '';

    $scope.edit = function(){
      //console.log('Editing user');
      //mvNotifier.notify('Editing!!');
      this.editing = !this.editing;
    }

    $scope.delete = function(user) {
      console.log('Deleting user');
      console.log(user);
    }

});
