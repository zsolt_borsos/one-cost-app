angular.module('app').factory('mvProductService', function(mvProduct, $q) {

//using new style for services

	var products = mvProduct.query();

	return {

		refresh : function() {
			/*
			mvProduct.query(function(products){
					this.products = products;
					return products;
			});
			*/
			return products = mvProduct.query();
		},

		getAll : function() {
			//return products;
			/*
			mvProduct.query(function(products){
					this.products = products;
					return products;
			});
			*/
			return products = mvProduct.query();
		},

		getProduct : function(productId) {
			//var dfd = $q.defer();
	    if (productId) {
	      for (var id in products){
	        //console.log(products[id]);
	        if (products[id].id === productId) {
	          //dfd.resolve(products[id]);
						return products[id];
	        }
	      }
				//return dfd.promise;

	    }
			//dfd.reject('Not found');
	  },

		getId : function(productId) {
	    if (productId) {
	      for (var id in products){
	        //console.log(products[id]);
	        if (products[id].id === productId) {
	          return id;
	        }
	      }
	    }
	  }

	}

});
