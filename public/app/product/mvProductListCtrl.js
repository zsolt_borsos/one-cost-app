angular.module('app').controller('mvProductListCtrl', function($scope, socket, mvSpinner, mvNotifier, mvProduct, mvProductDetail, mvSupplier, filterFilter) {

	//defaults
	$scope.records = mvProduct.query();
	$scope.productDetails = mvProductDetail.query();
	$scope.suppliers = mvSupplier.query();
	socket.emit('spinnerStart');
	$scope.searchText = '';
	$scope.origRecords = {};
	$scope.adding = false;
	$scope.recordCopy = undefined;
	$scope.currentRecord = undefined;
	$scope.entryLimit = 100; // items per page

	//update records as they arrive from the db
	$scope.getRecords = function(){

			//assign results to records
		$scope.records.$promise.then(function(result){
			$scope.origRecords = $scope.records = result;
			//console.log(result);
			console.log('loading details');
			$scope.addDetails(result);
			//set pagination settings
			$scope.currentPage = 1;
			$scope.totalItems = $scope.records.length;
			$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
			//stop spinner
			socket.emit('spinnerStop');
		});
	};

	//run 1st time for setup
	$scope.getRecords();

	//add description and packsize to records
	$scope.addDetails = function(records) {


		//needs finishing

	}

	$scope.paginate = function(value) {
			var begin, end, index;
			begin = ($scope.currentPage - 1) * $scope.entryLimit;
			end = begin + $scope.entryLimit;
			index = $scope.records.indexOf(value);
			return (begin <= index && index < end);
	};

	$scope.edit = function(record) {
		if ($scope.recordCopy != undefined) {
			$scope.cancel($scope.currentRecord);
			$scope.currentRecord.editing = false;
		}
		//console.log('Editing record');
		record.editing = true;
		$scope.currentRecord = record;
		$scope.recordCopy = angular.copy(record);

	};


	$scope.update = function(record) {
		//needs changing if description and packsize are added here as well
		mvProduct.update({id: record.id}, record);
		mvNotifier.notify('Product details updated.');
		$scope.recordCopy = undefined;
		record.editing = false;
		socket.emit('updateProducts');
	};

	$scope.add = function() {
		$scope.adding = true;
	};


	$scope.cancel = function(record) {
		var id = $scope.records.indexOf(record);
		$scope.records[id] = angular.copy($scope.recordCopy);
		$scope.records[id].editing = false;
		$scope.recordCopy = undefined;
	};

	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var deleted = record.$delete();
			deleted.then(function(success){
				mvNotifier.notify('Record deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
				socket.emit('updateProducts');
			}, function(error){
				mvNotifier.error('Failed to delete record...'+ error.statusText +' : '+ error.data);
			});
		}
	};


	$scope.resetSearch = function(){
		$scope.searchText = '';
	};


	$scope.$watch('searchText', function(newVal, oldVal){
		socket.emit('spinnerStart');
			console.log(newVal);

			if ((newVal === '') && (oldVal.length === 1)) {
				$scope.records = $scope.origRecords;
			}else{

				//mvNotifier.notify('Watch was triggered...');
				$scope.records = filterFilter($scope.origRecords, newVal);
				$scope.currentPage = 1;
				if ($scope.records) {
					$scope.totalItems = $scope.records.length;
					$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
				}
				socket.emit('spinnerStop');
			}
	});


	$scope.$on('finishedAdding', function(event, args) {
    $scope.adding = false;

  });


	socket.on('updateProducts', function() {
		console.log('Providing live data! Woohoo!!');
		$scope.getRecords();
	});


});
