angular.module('app').controller('mvProductMeasureAddCtrl', function($scope, mvNotifier, mvMeasure, socket) {

	$scope.measures = mvMeasure.query();
	$scope.selectedMeasure = {};
	$scope.valueMeasures = {};
	$scope.selectedValueMeasure = {};

	if ($scope.$parent.interpreted) {

		console.log($scope.$parent.selectedProduct);
		$scope.valueMeasures = {};
		$scope.selectedValueMeasure = {};
		$scope.value = '';
		$scope.unitOfSale = '';
	}

	$scope.resetState = function(){
		$scope.selectedMeasure = {};
		$scope.valueMeasures = {};
		$scope.selectedValueMeasure = {};
		$scope.value = '';
		$scope.unitOfSale = '';
	};

	$scope.loadState = function(state) {

	};

	$scope.cancel = function() {
		$('#measureModal').modal('hide');

	};

	$scope.create = function() {
		//create details object from form data
		var details = {};
		if ($scope.selectedMeasure && $scope.selectedValueMeasure && $scope.value && $scope.unitOfSale) {
			$('#measureModal').modal('hide');
			details.measureTypeId = $scope.selectedMeasure.id;
			details.measureValue = $scope.value * $scope.selectedValueMeasure.multi;
			details.unitOfSale = $scope.unitOfSale;
			//console.log(details);
			//notify UI
			$scope.$emit('addedMeasureDetails', details);
			mvNotifier.notify('Measure details added.');
		}else {
			mvNotifier.error('Please enter all the details in the form...');
		}
	};

	//populate dropdown values based on the 1st dropdown selection made on the form
	$scope.$watch('selectedMeasure', function(newVal, oldVal){
		$scope.valueMeasures = {};
		$scope.selectedValueMeasure = {};
		//console.log(newVal);
		var id = newVal.id;
		var list = [];

		//preset -> working with current db setup. Might need to change later...
		switch (id){
			case 1: list = [{multi: 1, name: 'g'}, {multi: 1000, name:'kg'}];
				break;
			case 2: list = [{multi: 1, name: 'ml'}, {multi: 1000, name: 'l'}];
				break;
			case 3: list = [{multi: 1, name: 'mm'}, {multi:10, name: 'cm'}, {multi: 1000, name:'m'}];
				break;
			case 4: list = [{multi:1, name:'Unit'}];
				break;
			default: list = [{name: 'Something went wrong?!'}];
		}

		$scope.valueMeasures = list;
	});

	$scope.$on('resetMeasureModal', function(event, args) {
		$scope.resetState();
	});
	//socket here?


});
