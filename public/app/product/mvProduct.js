angular.module('app').factory('mvProduct', function($resource) {

  var ProductResource = $resource('/api/v1/products/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return ProductResource;

});
