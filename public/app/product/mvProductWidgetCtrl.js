angular.module('app').controller('mvProductWidgetCtrl', function($q, $scope, $rootScope, $http, $timeout, mvNotifier, mvProduct, socket, filterFilter, mvProductDetail) {

	$scope.records = [];
	$scope.origRecords = {};
	$scope.searchText = '';
	$scope.currentPage = 1;
	$scope.entryLimit = 200; // items per page
	//$scope.pds = mvProductDetailService;

	$scope.resetSearch = function(){
		$scope.searchText = '';
	};

	$scope.$watch('searchText', function(newVal, oldVal){
		$scope.records = filterFilter($scope.origRecords, newVal);
		$scope.currentPage = 1;
		$scope.totalItems = $scope.records.length;
		$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);
	});

	$scope.paginate = function(value) {
			var begin, end, index;
			begin = ($scope.currentPage - 1) * $scope.entryLimit;
			end = begin + $scope.entryLimit;
			index = $scope.records.indexOf(value);
			return (begin <= index && index < end);
	};

	$scope.selectProduct = function(product){
		//might need to change this to keep socket communication to a minimum/use angular's own instead
		//for now it works
		socket.emit('selectedProduct', product);
	};

	$scope.getProductsFromServer = function() {
		//socket.emit('spinnerStart');
		var dfd = $q.defer();
		$scope.supplierId = $scope.$parent.selectedSupplier.id;
		if ($scope.supplierId) {
			var reqString = '/api/v1/suppliers/'+ $scope.supplierId +'/products';
			$http.get(reqString)
			.success(function(data, status, headers, config) {
					//$scope.records = $scope.origRecords = data;
					//using raw data, no need to convert just yet
					dfd.resolve(data);
					//create product instances
					$scope.totalItems = data.length;
					$scope.noOfPages = Math.ceil($scope.totalItems / $scope.entryLimit);

					//socket.emit('spinnerStop');
			  })
			.error(function(data, status, headers, config) {
			    console.log('Error with the server!');
					dfd.reject(data);
			 });

			return dfd.promise;

		}else {
			console.log('No supplierId in parent?');
		}
	};

	$scope.addDetails = function(records) {
		//console.log(records);

		//using native JS!
		var extended = [];
		for (var i = 0; i < records.length; i++) {
			var product = new mvProduct(records[i]);
			//console.log(product);
			product.details = mvProductDetail.get({productId: product.id});
			extended.push(product);
			//product.description = $scope.pds.pgetDesc(product.id);
			//product.packsize = $scope.pds.pgetPacksize(product.id);
			if (i === records.length-1) {
				$scope.records = extended;
				$scope.origRecords = extended;
				//console.log(extended);
				socket.emit('spinnerStop');
				return extended;
			}
		}

	};

	$scope.getProducts = function() {
		socket.emit('spinnerStart');
		console.log('scope trigger on widgetctrl');
		var gettingProducts = $scope.getProductsFromServer();
		gettingProducts.then(function(data){
				//console.log(data);
				mvNotifier.notify('Got products from database...');
				$scope.addDetails(data);
				//socket.emit('spinnerStop');
		});
	};


	socket.on('updateProductListForInvoiceManager', function() {
		mvNotifier.notify('Got request from socket to update product list!');
		$scope.getProducts();
	});


});
