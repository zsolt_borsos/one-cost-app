angular.module('app').controller('mvSalesTrackerCtrl', function($scope, $routeParams, socket, mvNotifier, mvPrice, mvSpinner) {

	socket.emit('spinnerStart');
	$scope.searchText = '';

	$scope.records = mvPrice.query(function(){

		socket.emit('spinnerStop');

	});


	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var deleted = record.$delete();
			deleted.then(function(success) {
				mvNotifier.notify('Record deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
			}, function(error){
				mvNotifier.error('Failed to delete record.');
			});
		}
	};


});
