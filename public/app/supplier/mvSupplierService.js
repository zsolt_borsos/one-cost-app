angular.module('app').factory('mvSupplierService', function(mvSupplier) {

	//using new style for services
	var suppliers = mvSupplier.query();

	return {

		refresh : function() {
			suppliers = mvSupplier.query();
			return suppliers;
		},

		getAll : function() {
			return suppliers;
		},

		getName : function(supplierId) {
	    if (supplierId) {
	      for (var id in suppliers){
	        //console.log(suppliers[id]);
	        if (suppliers[id].id === supplierId) {
	          return suppliers[id].name;
	        }
	      }
	    }
	  },

		getId : function(supplierId) {
	    if (supplierId) {
	      for (var id in suppliers){
	        //console.log(suppliers[id]);
	        if (suppliers[id].id === supplierId) {
	          return id;
	        }
	      }
	    }
	  }

	}

});
