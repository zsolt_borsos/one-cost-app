angular.module('app').controller('mvSupplierAddCtrl', function($scope, mvNotifier, mvSupplier, socket) {

	$scope.record = new mvSupplier;
	$scope.name = '';

	$scope.create = function() {
		if ($scope.name.length > 0) {
			$scope.record.name = $scope.name;
			$scope.record.note = $scope.note;
			$scope.record.defaultRebate = $scope.rebate;
			var added = $scope.record.$save();
			added.then(function(success){
				$scope.$emit('finishedAdding', true);
				mvNotifier.notify('Supplier added successfully.');
				socket.emit('updateSuppliers');
			}, function(error){
				mvNotifier.error('Failed to add record.');
			});
		}else {
			mvNotifier.error('Name is missing...');
		}
	};

	$scope.cancel = function() {
		$scope.$emit('finishedAdding', false);
	};

});
