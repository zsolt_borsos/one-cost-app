angular.module('app').factory('mvSupplier', function($resource) {

  var SupplierResource = $resource('/api/v1/suppliers/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return SupplierResource;

});
