angular.module('app').factory('mvLocation', function($resource) {

  var LocationResource = $resource('/api/v1/locations/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return LocationResource;

});
