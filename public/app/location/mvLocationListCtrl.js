angular.module('app').controller('mvLocationListCtrl', function($scope, mvNotifier, mvLocation, mvCountyService) {

	$scope.records = mvLocation.query();
	$scope.searchText = '';
	$scope.adding = false;
	$scope.recordCopy = undefined;
	$scope.currentRecord = undefined;
	//$scope.selectedCounty = undefined;

	//county
	$scope.counties = mvCountyService.getAll();

	$scope.getCountyName = mvCountyService.getCountyName;
	//$scope.getCounty = mvCountyService.getCounty;

	$scope.edit = function(record) {
		if ($scope.recordCopy != undefined) {
			$scope.cancel($scope.currentRecord);
			$scope.currentRecord.editing = false;
		}
		//console.log('Editing record');
		record.editing = true;
		$scope.currentRecord = record;
		//console.log(record.countyId);
		var selectedId = mvCountyService.getCountyId(record.countyId);
		record.selectedCounty = $scope.counties[selectedId];
		$scope.recordCopy = angular.copy(record);
	};


	$scope.update = function(record) {
		if (record.selectedCounty){
			record.countyId = record.selectedCounty.id;
		}else {
			record.countyId = null;
		}
		//console.log(record);
		mvLocation.update({id: record.id}, record);
		mvNotifier.notify('Location details updated.');
		$scope.recordCopy = undefined;
		record.editing = false;
	};

	$scope.add = function() {
		$scope.adding = true;
	};


	$scope.cancel = function(record) {
		var id = $scope.records.indexOf(record);
		$scope.records[id] = angular.copy($scope.recordCopy);
		$scope.records[id].editing = false;
		$scope.recordCopy = undefined;
	};

	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var deleted = record.$delete();
			deleted.then(function(success){
				mvNotifier.notify('Record deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
			}, function(error){
				mvNotifier.error('Failed to delete record...'+ error.statusText +' : '+ error.data);
			});
		}
	};

	$scope.$on('finishedAdding', function(event, args) {
    $scope.adding = false;
		if (args == true) {
			$scope.records = mvLocation.query();
		}
  });

});
