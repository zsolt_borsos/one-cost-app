angular.module('app').factory('mvLocationService', function(mvLocation) {

	var locations = mvLocation.query();

	return {

		refresh : function() {
			locations = mvLocation.query();
			return locations;
		},

		getAll : function() {
			return locations;
		},

		getLocationName : function(locationId) {
	    if (locationId) {
	      for (var id in locations){
	        //console.log(locations[id]);
	        if (locations[id].id === locationId) {
	          return locations[id].name;
	        }
	      }
	    }
	  },

		getLocationId : function(locationId) {
	    if (locationId) {
	      for (var id in locations){
	        //console.log(locations[id]);
	        if (locations[id].id === locationId) {
	          return id;
	        }
	      }
	    }
	  }

	}

});
