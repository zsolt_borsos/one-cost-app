angular.module('app').controller('mvClientManagerCtrl', function($scope, mvNotifier, mvClient, $location) {

	$scope.clients = mvClient.query();
	$scope.searchText = '';
	$scope.currentRecord = undefined;
	$scope.selectedTab = 'detailsTab';
	$scope.update = {};


	$scope.selectClient = function(client) {

		$scope.currentRecord = client;
		console.log($scope.currentRecord);
		
	}

	$scope.selectTab = function(tabName) {
		$scope.selectedTab = tabName;
	}

	$scope.clearSearch = function() {
		$scope.searchText = '';
	};



});
