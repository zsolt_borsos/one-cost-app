angular.module('app').controller('mvInvoiceManagerCtrl', function($scope, socket, $routeParams, mvNotifier, mvInvoice, mvSupplierService, mvSupplier, mvProduct ) {

	//$scope.invoiceDate && $scope.selectedSupplier
	$scope.invoiceId = $routeParams.invoiceId;
	$scope.suppliers = mvSupplierService.getAll();
	$scope.invoices = mvInvoice.query();
	$scope.selectedSupplier = undefined;
	$scope.detailsSet = false;
	$scope.invoiceItemsAdded = false;
	//$scope.invoiceDate = undefined;
	$scope.showDatePicker = false;
	$scope.ccId = $routeParams.ccId;
	$scope.imageUrl = '/api/v1/invoices/' + $scope.invoiceId + '/image';
	$scope.angle = 0;
	$scope.selectedProduct = {};
	$scope.currentInvoice = {};

	$scope.invoices.$promise.then(function(data){
		//$scope.invoices.forEach(function(invoice){
		data.forEach(function(invoice){
			//console.log(invoice);
			//console.log($scope.invoiceId);
			if (invoice.id == $scope.invoiceId) {
				$scope.currentInvoice = invoice;
				console.log('invoice:', invoice);
				var invoiceDate = new Date($scope.currentInvoice.date);
				$scope.invoiceDate = invoiceDate.toDateString();
				$scope.supplierId = invoice.supplierId;
				$scope.total = $scope.currentInvoice.total;
				$scope.suppliers.forEach(function(supplier){
					if (supplier.id === invoice.supplierId) {
						$scope.selectedSupplier = supplier;
						$scope.detailsSet = true;
						//socket.emit('updateProductListForInvoiceManager');
					}
				});

			}
		});
	});


	$scope.setDetails = function() {
		if ($scope.detailsSet) {
			$scope.detailsSet	= false;

		}else {
			if ($scope.invoiceDate && $scope.selectedSupplier) {
				//console.log($scope.invoiceDate);
				//var invoice = new mvInvoice($scope.currentInvoice);
				$scope.currentInvoice.date = $scope.invoiceDate;
				console.log('current invoice details:', $scope.currentInvoice);
				$scope.currentInvoice.supplierId = $scope.selectedSupplier.id;
				$scope.currentInvoice.total = $scope.total;
				//invoice.$update();
				mvInvoice.update({id: $scope.invoiceId}, $scope.currentInvoice);
				$scope.detailsSet = true;
				mvNotifier.notify('Details updated...');

			}else{
				mvNotifier.error('Please select a supplier and set the invoice date...');
			}
		}
	};

	$scope.toggleDatePicker = function() {
		$scope.showDatePicker = !$scope.showDatePicker;
	};

	$scope.dateEntered = function() {
		$scope.showDatePicker = false;
	};

	$scope.rotateImage = function() {
		var img = angular.element(image);
		//console.log(img);
		$scope.angle = ($scope.angle+90)%360;
		img.removeClass();
		var className = "rotate" + $scope.angle;
		img.addClass(className);
	};



	$scope.updateSelectedProduct = function() {
		var product = new mvProduct;
		console.log($scope.selectedProduct);
		//if it is an existing product then write measure details in the db straight from here
		if ($scope.selectedProduct.description) {
			angular.extend(product, $scope.selectedProduct);
			var updated = product.$update();
			updated.then(function(success){
				mvNotifier.notify('Product details updated.');
			}, function(error){
				mvNotifier.error('Failed to update record...'+ error.statusText +' : '+ error.data);
			});
		}else {
			//if it is a new product then we do not create the product just yet
			console.log('New product detected, Product is not saved.');
		}
	};

	$scope.finishInvoice = function(){
		if ($scope.currentInvoice.done) {
			$scope.currentInvoice.done = false;
			mvInvoice.update({id: $scope.invoiceId}, $scope.currentInvoice);
			mvNotifier.notify('Invoice is not finished...');
		}else {
			$scope.currentInvoice.done = true;
			mvInvoice.update({id: $scope.invoiceId}, $scope.currentInvoice);
			mvNotifier.notify('Invoice is finished.');
		}
	};

	$scope.$on('finishedAdding', function(event, args) {
		$('#suppModal').modal('hide');
	});

	$scope.$on('loading', function(event, args) {
		$scope.loading = args;
		if (args) {
			socket.emit('spinnerStart');
		}else {
			socket.emit('spinnerStop');
		}
	});


	$scope.$on('addedMeasureDetails', function(event, args) {
		angular.extend($scope.selectedProduct, args);
		$scope.updateSelectedProduct();
		$scope.$broadcast('updateMeasureDetails');
	});

	$scope.$on('productsUpdated', function(event, args) {
		//$scope.$broadcast('updateProducts');
		console.log('Ignoring this for now. Using socket instead.!');
	});

/*
	$scope.$on('addedInvoiceItem', function(event, args) {
		$scope.$broadcast('updateInvoiceItems');
	});
*/

	/*
	$scope.$watch('invoiceItemsAdded', function(newValue, oldValue) {
		mvNotifier.notify('invoiceItemsAdded was: ' + oldValue + ' changed to: ' + newValue);
		//console.log(newValue);
		//console.log(oldValue);
	});
	*/


	$scope.$watch('detailsSet', function(newValue, oldValue) {
		mvNotifier.notify('Details are changed from ' + oldValue + ' to ' + newValue);
		if (newValue === true) {
			socket.emit('updateProductListForInvoiceManager');
		}
	});

	/*
	$scope.$on('updateProductList', function(event, args) {
		$scope.$broadcast('getProducts');
	});
	*/

	$scope.$on('resetSelectedProduct', function(event, args) {
		$scope.selectedProduct = {};
		$scope.$broadcast('resetMeasureModal');
	});

	$scope.$on('InvoiceDetailsSet', function(event, args) {
		$scope.detailsSet = true;
		//$scope.invoiceDate && $scope.selectedSupplier
		$scope.suppliers.forEach(function(supplier){
			//console.log(supplier);
			if (supplier.id === args) {
				$scope.selectedSupplier = supplier;
			}
		});

	});

	socket.on('updateSuppliers', function() {
		console.log('Providing live data! Woohoo!!');
		$scope.suppliers = mvSupplierService.refresh();
	});

	socket.on('setSelectedProduct', function(product) {
		$('#productsModal').modal('hide');
		console.log('Providing live data! Woohoo!!');
		$scope.selectedProduct = product;
		console.log(product);
	});




});
