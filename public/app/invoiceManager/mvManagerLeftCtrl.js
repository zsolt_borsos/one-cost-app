angular.module('app').controller('mvManagerLeftCtrl', function($scope, mvNotifier, socket, mvSpinner, mvProduct, mvProductDetail, mvPrice, mvInvoiceItem) {

	//defaults
	$scope.productCodeChecked = true;
	$scope.packsizeChecked = true;
	$scope.interpreted = false;
	$scope.newProduct = true;
	$scope.selectedProduct = new mvProduct;
	//on parent: ccId, invoiceId, selectedSupplier,

	socket.on('setSelectedProduct', function(product) {
		$scope.selectedProduct = new mvProduct(product);
		//console.log(product);
		$scope.selectedProduct.supplierId = $scope.$parent.selectedSupplier.id;
		$scope.selectedProduct.productCode = product.productCode;
		$scope.selectedProduct.description = product.details.description;
		$scope.selectedProduct.packsize = product.details.packsize;
		$scope.newProduct = false;
		if (product.measureValue) {
			$scope.interpreted = true;
		}
	});


	$scope.$on('updateMeasureDetails', function(event, args) {
		if ($scope.$parent.selectedProduct.measureTypeId) {
			$scope.interpreted = true;
			$scope.selectedProduct.supplierId = $scope.$parent.selectedSupplier.id;
			angular.extend($scope.selectedProduct, $scope.$parent.selectedProduct);
		}else {
			$scope.interpreted = false;
		}
	});

	$scope.resetStatus = function() {
		$scope.productCodeChecked = true;
		$scope.packsizeChecked = true;
		$scope.interpreted = false;
		$scope.newProduct = true;
		$scope.selectedProduct = new mvProduct;
		$scope.$emit('resetSelectedProduct');
	};

	$scope.interpretPacksize = function(description, packsize){

		if (!description) {
			description = '';
		}
		if (!packsize) {
			packsize = '';
		}

		$('#measureModal #desc').html('<b>Description: </b>' + description);
		$('#measureModal #packsize').html('<b>Packsize: </b>' + packsize);
		$('#measureModal').modal('show');
	};

	//save details to db
	$scope.addProduct = function(){

		if (!($scope.interpreted && $scope.selectedProduct.description && $scope.selectedProduct.qty && $scope.selectedProduct.totalPaid)) {
			mvNotifier.error('Please enter all the details in the form...')
			return;
		}
		socket.emit('spinnerStart');
		//if it is a new product then create new records in db
		if ($scope.newProduct) {
			//generate productcode if needed
			if (!$scope.selectedProduct.productCode) {
				$scope.selectedProduct.productCode = 'temp';
			}
			var saved = $scope.selectedProduct.$save();
			saved.then(function(product){
				//var product = product;

				// if it is a temp productCode then change it to the product's ID
				if (product.productCode == 'temp') {
					//console.log('Need to change temp to id...');
					mvProduct.get({id:product.id}, function(p) {
  					p.productCode = p.id;
						//console.log(p);
  					p.$update();
						console.log('Updating productCode.');
					});
				}

				mvNotifier.notify('Saved product.');
				//console.log('Success item:');
				//console.log(product);
				//add product details to db
				var productDetails = new mvProductDetail;
				productDetails.description = product.description;
				productDetails.packsize = product.packsize;
				productDetails.productId = product.id;

				var addedDetails = productDetails.$save();
				//add price details to db
				//add current item to invoice item
				addedDetails.then(function(result){
					mvNotifier.notify('Saved Details.');

					var invoiceItem = new mvInvoiceItem;
					invoiceItem.invoiceId = $scope.$parent.invoiceId;
					invoiceItem.productId = product.id;
					invoiceItem.qty = $scope.selectedProduct.qty;
					var addedItem = invoiceItem.$save();
					addedItem.then(function(done){
						console.log(done);
						mvNotifier.notify('Saved InvoiceItem.');

						var price = new mvPrice;
						price.productId = product.id;
						price.dateFrom = $scope.$parent.invoiceDate;
						price.value = $scope.selectedProduct.totalPaid / $scope.selectedProduct.qty;
						//price file type ID -> from db.
						price.sourceTypeId = 3;
						console.log('expecting insertedId here!:');
						console.log(done);
						price.sourceValue = done.id;
						price.note = 'invoiceItemId in sourceValue';
						var priceAdded = price.$save();

						priceAdded.then(function(data){
							mvNotifier.notify('Saved Price.');


							//resetting form details
							$scope.resetStatus();
							//$scope.$emit('productsUpdated');
							socket.emit('updateInvoiceItems');
							socket.emit('updateProductListForInvoiceManager');
							socket.emit('spinnerStop');

						}, function(error) {
							console.log(error);
							mvNotifier.error('Failed to add price.');
						});

					}, function(error) {
						console.log(error);
						mvNotifier.error('Failed to add invoice item.');
					});

				}, function(error){
					console.log(error);
					mvNotifier.error('Failed to save product details.');
				});

			}, function(error){
				mvNotifier.error('Failed to save product.');
			});
		}else {
			//if the product already exists then just update records in db

			//updating product in db
			var saved = $scope.selectedProduct.$update();
			saved.then(function(product){
				//updating productDetails in db
				mvNotifier.notify('Updated Product.');
				console.log(product);
				var productDetails = mvProductDetail.get({productId: product.id});

				productDetails.$promise.then(function(productDetails){
					console.log('details:');
					console.log(productDetails);
					productDetails.description = product.description;
					productDetails.packsize = product.packsize;
					productDetails.productId = product.id;

					var updateDetails = productDetails.$update();

					updateDetails.then(function(result){

					//add price details to db
					//add current item to invoice item
					//addedDetails.
						mvNotifier.notify('Updated Details.');

						var invoiceItem = new mvInvoiceItem;
						invoiceItem.invoiceId = $scope.$parent.invoiceId;
						invoiceItem.productId = product.id;
						invoiceItem.qty = $scope.selectedProduct.qty;
						var addedItem = invoiceItem.$save();
						addedItem.then(function(done){
							console.log(done);
							mvNotifier.notify('Saved InvoiceItem.');

							var price = new mvPrice;
							price.productId = product.id;
							price.dateFrom = $scope.$parent.invoiceDate;
							price.value = $scope.selectedProduct.totalPaid / $scope.selectedProduct.qty;
							//price file type ID -> from db.
							price.sourceTypeId = 3;
							price.sourceValue = done.id;
							price.note = 'invoiceItemId in sourceValue';

							var priceAdded = price.$save();
							priceAdded.then(function(data){
								mvNotifier.notify('Saved Price.');

							//resetting form details
							$scope.resetStatus();
							//$scope.$emit('productsUpdated');
							socket.emit('updateInvoiceItems');
							socket.emit('updateProductListForInvoiceManager');
							socket.emit('spinnerStop');

						});
						});
					});
				});
			});
		}
	};



});
