angular.module('app').factory('mvPrice', function($resource) {

  var PriceResource = $resource('/api/v1/prices/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return PriceResource;

});
