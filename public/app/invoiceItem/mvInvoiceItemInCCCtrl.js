angular.module('app').controller('mvInvoiceItemInCCCtrl', function($scope, socket, $timeout, $routeParams, mvNotifier, mvInvoiceItem, mvProductDetailService, mvProduct, mvPrice) {

	$scope.searchText = '';
	$scope.ccId = $routeParams.id;

	$scope.constructme = function() {
		socket.emit('spinnerStart');
		$scope.prices = mvPrice.query({typeId: 3});
		var items = mvInvoiceItem.query({ccId: $scope.ccId});

		items.$promise.then(function(data){
			$scope.records = data;
			//console.log(data);
			$scope.prices.$promise.then(function(){
				$scope.records.$promise.then(function(){


					for (var i = 0; i < $scope.records.length; i++) {
						var record = $scope.records[i];
						//console.log(record);
						var product = mvProduct.get({id: record.productId});
						record.description = mvProductDetailService.pgetDesc(record.productId);
						record.packsize = mvProductDetailService.pgetPacksize(record.productId);
						record.product = product;
						//console.log($scope.prices);

						$scope.prices.forEach(function(price){
							//console.log(price);
							if (parseInt(price.sourceValue) === record.id) {
								record.price = price.value;
								return;
							}
						});

						$scope.records[i] = record;
						if (i === $scope.records.length) {
							return $scope.records;
						}
						//console.log(record);
					}
				});
		});
		}, function(error){
			console.log('error:');
			console.log(error);
		}).then(function(data){

			socket.emit('spinnerStop');
		});

	};

	//$scope.constructme();

	socket.emit('spinnerStart');
	$timeout(function() {
		//socket.emit('spinnerStart');
		$scope.constructme();
	}, 2000);


	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var deleted = record.$delete();
			deleted.then(function(success) {
				mvNotifier.notify('Record deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
			}, function(error){
				mvNotifier.error('Failed to delete record...'+ error.statusText +' : '+ error.data);
			});
		}
	};


	socket.on('updateInvoiceItems', function(){
		socket.emit('spinnerStart');
		mvNotifier.notify('Updating invoice items list for invoice manager views...');
		//$scope.products = mvProductService.refresh();
		//$scope.products.$promise.then(function(){
			$scope.details = mvProductDetailService.refresh();
			$scope.details.$promise.then(function(){
				$scope.constructme();
				socket.emit('spinnerStop');
			});
		//});
	});

});
