angular.module('app').factory('mvInvoiceItemService', function(mvInvoiceItem) {

//using new style for services

	var invoiceItems = mvInvoiceItem.query();

	return {

		refresh : function() {
			return invoiceItems = mvInvoiceItem.query();
			//return invoiceItems;
		},

		getAll : function() {
			return invoiceItems;
		},
		/*
		getName : function(invoiceItemId) {
	    if (invoiceItemId) {
	      for (var id in invoiceItems){
	        //console.log(invoiceItems[id]);
	        if (invoiceItems[id].id === invoiceItemId) {
	          return invoiceItems[id].name;
	        }
	      }
	    }
	  },
		*/
		getId : function(invoiceItemId) {
	    if (invoiceItemId) {
	      for (var id in invoiceItems){
	        //console.log(invoiceItems[id]);
	        if (invoiceItems[id].id === invoiceItemId) {
	          return id;
	        }
	      }
	    }
	  }

	}

});
