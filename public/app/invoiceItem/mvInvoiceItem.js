angular.module('app').factory('mvInvoiceItem', function($resource) {

  var InvoiceItemResource = $resource('/api/v1/invoiceItems/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return InvoiceItemResource;

});
