angular.module('app').controller('mvCCListCtrl', function($scope, mvNotifier, mvCC, mvClient, mvClientService, socket) {

	$scope.records = mvCC.query();
	$scope.searchText = '';
	$scope.adding = false;
	$scope.recordCopy = undefined;
	$scope.currentRecord = undefined;
	//$scope.selectedClient = undefined;

	$scope.clients = mvClient.query();
	$scope.getClientName = mvClientService.getName;

	$scope.edit = function(record) {
		if ($scope.recordCopy != undefined) {
			$scope.cancel($scope.currentRecord);
			$scope.currentRecord.editing = false;
		}
		//console.log('Editing record');
		var pos = mvClientService.getId(record.clientId);
		record.selectedClient = $scope.clients[pos];
		record.editing = true;
		$scope.currentRecord = record;
		$scope.recordCopy = angular.copy(record);

	};

	$scope.update = function(record) {
		if (record.selectedClient){

			record.clientId = record.selectedClient.id;
			mvCC.update({id: record.id}, record);
			socket.emit('updateCC');
			mvNotifier.notify('CC details updated.');
			$scope.recordCopy = undefined;
			record.editing = false;
		}
	};

	$scope.add = function() {
		$scope.adding = true;
	};


	$scope.cancel = function(record) {
		var id = $scope.records.indexOf(record);
		$scope.records[id] = angular.copy($scope.recordCopy);
		$scope.records[id].editing = false;
		$scope.recordCopy = undefined;
	};

	$scope.delete = function(record) {
		var sure = confirm('Are you sure you want to delete this record?');
		if (sure){
			var deleted = record.$delete();
			deleted.then(function(success) {
				mvNotifier.notify('Record deleted.');
				var id = $scope.records.indexOf(record);
				$scope.records.splice(id, 1);
				socket.emit('updateCC');
			}, function(error){
				mvNotifier.error('Failed to delete record...'+ error.statusText +' : '+ error.data);
			});
		}
	};

	$scope.$on('finishedAdding', function(event, args) {
    $scope.adding = false;
		if (args == true) {
			socket.emit('updateCC');
			mvNotifier.globalNotify('A new CC was just created.');
			$scope.records = mvCC.query();
		}
  });

	socket.on('updateCC', function() {
		console.log('Providing live data! Woohoo!!');
    $scope.records = mvCC.query();
  });

	socket.on('updateClients', function() {
		console.log('Updating client data in cc page');
		$scope.clients = mvClient.query();
  });



});
