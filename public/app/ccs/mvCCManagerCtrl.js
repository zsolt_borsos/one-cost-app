angular.module('app').controller('mvCCManagerCtrl', function($scope, socket, $routeParams, mvNotifier, mvCC, mvCCService, mvClientService, mvSpinner) {

//on this page:
//invoices->invoice items -> products
	$scope.ccId = $routeParams.id;

	//spinner trigger
	$scope.loading = false;

	$scope.ccs = mvCC.query();
		//console.log(success);

	mvCC.get({id:$scope.ccId})
		    .$promise.then(function(cc) {
		      $scope.currentCC = cc;
		    });

	$scope.getClientName = mvClientService.getName;


	$scope.$on('invoiceAdded', function(event, args) {
		console.log('event noticed in cc manager!');
		if (args == true) {
			$scope.$broadcast('updateInvoices');
		}
	});


	$scope.$on('loading', function(event, args) {
		$scope.loading = args;
		if (args) {
			socket.emit('spinnerStart');
		}else {
			socket.emit('spinnerStop');
		}
	});



});
