angular.module('app').factory('mvCC', function($resource) {

  var CcResource = $resource('/api/v1/ccs/:id', {id: '@id'}, {
    update: { method: 'PUT', isArray: false }
  });

  return CcResource;

});
