angular.module('app').factory('mvCCService', function(mvCC) {

	var ccs = mvCC.query();

	return {

		refresh : function() {
			ccs = mvCC.query();
			return ccs;
		},

		getAll : function() {
			return ccs;
		},

		/* not used, maybe add it in db? Later...

		getName : function(ccId) {
	    if (ccId) {
	      for (var id in ccs){
	        //console.log(ccs[id]);
	        if (ccs[id].id === ccId) {
	          return ccs[id].name;
	        }
	      }
	    }else {
	      return "Not specified.";
	    }
	  },
		*/
		getId : function(ccId) {
	    if (ccId) {
	      for (var id in ccs){
	        //console.log(ccs[id]);
	        if (ccs[id].id === ccId) {
	          return id;
	        }
	      }
	    }
	  }

	}



});
