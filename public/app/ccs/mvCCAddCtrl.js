angular.module('app').controller('mvCCAddCtrl', function($scope, mvNotifier, mvCC, mvClient) {

	$scope.record = new mvCC;

	$scope.clients = mvClient.query();
	$scope.selectedClient = undefined;
	$scope.createRecord = function() {
		if ($scope.selectedClient) {

			$scope.record.note = $scope.note;
			if ($scope.selectedClient) {
				$scope.record.clientId = $scope.selectedClient.id;
			}
			var added = $scope.record.$save();
			added.then(function(success){
				$scope.$emit('finishedAdding', true);
				mvNotifier.notify('CC created successfully.');
			}, function(error){
				mvNotifier.error('Failed to create CC.');
			});
		}else {
			mvNotifier.error('Select a client...');
		}
	};

	$scope.cancel = function() {
		$scope.$emit('finishedAdding', false);
	};

});
